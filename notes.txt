https://stackabuse.com/building-a-rest-api-with-node-and-express/
https://github.com/jkasun/simple-rest-sever-express

touch index.js

npm install --save express

node index.js

npm install --save cors

postgres:
npm install express sequelize pg pg-hstore --save

neu typescript: -----wieder deinstalliert
npm install reflect-metadata sequelize-typescript --save

npm install ts-node --save-dev
npm install typescript ts-node-dev @types/node --save-dev

npm install @types/express @types/validator --save-dev



============ es6:
After creating project install node package run command:

npm install --save @babel/polyfill

install node dev dependencies package run command:

npm install --save-dev @babel/core @babel/cli @babel/preset-env @babel/node nodemon

npm install --save-dev @babel/preset-typescript 
npm install --save-dev @babel/plugin-proposal-class-properties @babel/plugin-proposal-object-rest-spread




# install locally (recommended)
npm install dotenv --save

import * as dotenv from "dotenv"; // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
dotenv.config();




npm install --save-dev webpack webpack-cli
npm install --save-dev babel-loader css-loader style-loader
npm install --save core-js


npm install sequelize-typescript --save
npm i --save-dev typescript @types/express @types/node tslib@latest
npm install --save-dev @babel/plugin-transform-typescript

npm install --save core-js regenerator-runtime
npm install --save @overnightjs/core
npm install --save http-status-codes

====================== xslt ===========================================================

https://github.com/Saxonica/SaxonJS-Tutorial-2021/tree/main/exercises/ex03
https://www.saxonica.com/saxon-js/documentation2/index.html#!starting/export/compiling-using-XX
npm install --save-dev xslt3
xslt3 -xsl:datasetxml2oai-pmh.xslt -export:datasetxml2oai.sef.json -t
./node_modules/xslt3/xslt3.js -xsl:datasetxml2oai-pmh.xslt -export:src/controllers/datasetxml2oai.sef.json -t -nogo "-ns:##html5"

npm install --save saxon-js
=============================================================================================


npm install --save-dev prettier
npm install --save-dev eslint eslint-config-prettier eslint-plugin-prettier @typescript-eslint/eslint-plugin @typescript-eslint/parser

https://khalilstemmler.com/blogs/tooling/prettier/ :
    eslint-config-prettier: Turns off all ESLint rules that have the potential to interfere with Prettier rules.
    eslint-plugin-prettier: Turns Prettier rules into ESLint rules.
==========================================================================================



npm install --save xmlbuilder2




====================== redis
npm install --save redis



npm i sprintf-js --save
npm i --save-dev @types/sprintf-js







======================================== git =============================================================
git config --global user.name
git config --global user.email

git remote add origin https://gitea.geologie.ac.at/geolba/tethys.api.git 
git push -o repo.private=false --set-upstream origin master



npm install --save multer

curl -X POST -d "verb=identify" -H "Content-Type: application/xml" http://localhost:3000/oai
curl -X POST -H "Content-Type: application/xml" -d '{"verb": "Identify"}' http://localhost:8080/oai
import { Dialect } from "sequelize";
import { Sequelize } from "sequelize-typescript";
// import * as dotenv from "dotenv"; // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
// dotenv.config();
// import 'dotenv/config';
import dotenv from "dotenv";
dotenv.config();
// import { Dataset } from '../models/Dataset';
// import { Abstract } from "../models/Abstract";
// import DocumentXmlCache from '../models/DocumentXmlCache';

// const dbName = process.env.DB_NAME as string
// const dbUser = process.env.DB_USER as string
// const dbHost = process.env.DB_HOST
// const dbDriver = process.env.DB_DRIVER as Dialect
// const dbPassword = process.env.DB_PASSWORD

const dbSchema = process.env.DB_SCHEMA;
const dbName = process.env.DB_NAME as string;
const dbUser = process.env.DB_USER as string;
const dbPassword = process.env.DB_PASSWORD;
const dbHost = process.env.DB_HOST;
const dbDriver = process.env.DB_DRIVER as Dialect;

const sequelizeConnection = new Sequelize(dbName, dbUser, dbPassword, {
    schema: dbSchema,
    host: dbHost || "localhost",
    port: 5432,
    dialect: dbDriver || "postgres",
    dialectOptions: {
        ssl: process.env.DB_SSL == "true",
        useUTC: false, //for reading from database
        dateStrings: true,
        typeCast: true,
    },
    pool: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000,
    },
    logging: false,
    timezone: "Europe/Vienna", //for writing to database
});
// sequelizeConnection.addModels([Dataset, Abstract]);
// sequelizeConnection.addModels([DocumentXmlCache]);

export const initDB = async () => {
    await sequelizeConnection.authenticate();
    // .then(() => {
    //   console.log("Connection has been established successfully.");
    // })
    // .catch((err) => {
    //   console.error("Unable to connect to the database:", err);
    // });
};

export default sequelizeConnection;

// export { Dataset, Abstract };

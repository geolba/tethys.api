import { Controller, Get } from "@overnightjs/core";
import dbContext from "../models/init-models.js";
import { Dataset, User, Person } from "../models/init-models.js";
import Sequelize from "sequelize";
const Op = Sequelize.Op;
import { NextFunction, Request, Response } from "express";
// import Logger from 'jet-logger';
import { StatusCodes } from "http-status-codes";

@Controller("api/dataset")
export class DatasetController {
    @Get("")
    public async findAll(req: Request, res: Response) {
        // const type = req.query.type;
        // var condition = type ? { type: { [Op.iLike]: `%${type}%` } } : null;,
        const server_state = "published";
        const condition = { server_state: { [Op.eq]: `${server_state}` } };

        Dataset.findAll({
            where: condition,
            include: ["abstracts"],
            order: ["server_date_published"],
        })
            .then((data) => {
                res.status(StatusCodes.OK).send(data);
            })
            .catch((err) => {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving datasets.",
                });
            });
    }

    @Get(":publish_id")
    public async findOne(req: Request, res: Response, next: NextFunction) {
        const publish_id = req.params.publish_id;

        try {
            const dataset = await dbContext.Dataset.findOne({
                where: { publish_id: publish_id },
                include: [
                    "titles",
                    "abstracts",
                    {
                        model: User,
                        as: "user",
                    },
                    {
                        model: Person,
                        through: { where: { role: "author" } },
                        as: "authors",
                        // order: [['link_documents_persons.sort_order', 'ASC']],
                    },
                    {
                        model: Person,
                        through: { where: { role: "contributor" } },
                        as: "contributors",
                    },

                    "subjects",
                    "coverage",
                    "licenses",
                    "references",
                    // "referenced_by",
                    "project",
                    {
                        model: dbContext.Reference,
                        as: "referenced_by",
                        include: [
                            {
                                model: dbContext.Dataset,
                                as: "dataset",
                                foreignKey: "document_id",
                                include: ["identifier"],
                            },
                        ],
                    },
                    "files",
                    // {
                    //     model: dbContext.File,
                    //     as: "files",
                    //     include: ["hashvalues"],
                    // },
                    "identifier",
                ],
                order: [
                    ["authors", dbContext.DocumentPersons, "sort_order", "ASC"],
                    ["contributors", dbContext.DocumentPersons, "sort_order", "ASC"],
                ],
                // order: ['server_date_published'],
                // order: ['server_date_published'],
                // logging: (sql, queryObject) => {
                //     const test = sql;
                //     console.log(sql);
                // },
            });
            if (dataset) {
                // dataset.files = await dataset.getFiles();
                res.status(StatusCodes.OK).send(dataset);
            } else {
                res.status(StatusCodes.NOT_FOUND).send({
                    message: `Cannot find Dataset with publish_id=${publish_id}.`,
                });
            }
        } catch (error) {
            // res.status(500).send({
            //     message: "Error retrieving Dataset with publish_id=" + publish_id,
            // });
            return next(error);
        }

        // .then((data) => {
        //     if (data) {
        //         res.send(data);
        //     } else {
        //         res.status(404).send({
        //             message: `Cannot find Dataset with publish_id=${publish_id}.`,
        //         });
        //     }
        // })
        // .catch((err) => {
        //     res.status(500).send({
        //         message: "Error retrieving Dataset with publish_id=" + publish_id,
        //     });
        // });
        // if (dataset) {
        //     res.status(StatusCodes.OK).send(dataset);
        // } else {
        //     res.status(StatusCodes.NOT_FOUND).send({
        //         message: `Cannot find Dataset with publish_id=${publish_id}.`,
        //     });
        // }
    }

    @Get(":prefix/:value") // 10.24341/tethys.99.2
    public async findByIdentifier(req: Request, res: Response, next: NextFunction) {
        const value = req.params.prefix + "/" + req.params.value;

        try {
            const dataset = await dbContext.Dataset.findOne({
                where: {
                    // id: Sequelize.literal(`(SELECT dataset_id FROM gba.dataset_identifiers WHERE value = '${value}')`),
                    id: {
                        [Sequelize.Op.in]: Sequelize.literal(`(SELECT dataset_id FROM gba.dataset_identifiers WHERE value = '${value}')`),
                    },
                },
                include: [
                    "titles",
                    "abstracts",
                    {
                        model: User,
                        as: "user",
                    },
                    {
                        model: Person,
                        through: { where: { role: "author" } },
                        as: "authors",
                        // order: [['link_documents_persons.sort_order', 'ASC']],
                    },
                    {
                        model: Person,
                        through: { where: { role: "contributor" } },
                        as: "contributors",
                    },

                    "subjects",
                    "coverage",
                    "licenses",
                    "references",
                    // "referenced_by",
                    "project",
                    {
                        model: dbContext.Reference,
                        as: "referenced_by",
                        include: [
                            {
                                model: dbContext.Dataset,
                                as: "dataset",
                                foreignKey: "document_id",
                                include: ["identifier"],
                            },
                        ],
                    },
                    "files",
                    // {
                    //     model: dbContext.File,
                    //     as: "files",
                    //     include: ["hashvalues"],
                    // },
                    "identifier",
                ],
                order: [
                    ["authors", dbContext.DocumentPersons, "sort_order", "ASC"],
                    ["contributors", dbContext.DocumentPersons, "sort_order", "ASC"],
                ],
                // order: ['server_date_published'],
                // order: ['server_date_published'],
            });
            if (dataset) {
                res.status(StatusCodes.OK).send(dataset);
            } else {
                res.status(StatusCodes.NOT_FOUND).send({
                    message: `Cannot find Dataset with doi_value=${value}.`,
                });
            }
        } catch (error) {
            // res.status(500).send({
            //     message: "Error retrieving Dataset with publish_id=" + publish_id,
            // });
            return next(error);
        }

        // .then((data) => {
        //     if (data) {
        //         res.send(data);
        //     } else {
        //         res.status(404).send({
        //             message: `Cannot find Dataset with publish_id=${publish_id}.`,
        //         });
        //     }
        // })
        // .catch((err) => {
        //     res.status(500).send({
        //         message: "Error retrieving Dataset with publish_id=" + publish_id,
        //     });
        // });
        // if (dataset) {
        //     res.status(StatusCodes.OK).send(dataset);
        // } else {
        //     res.status(StatusCodes.NOT_FOUND).send({
        //         message: `Cannot find Dataset with publish_id=${publish_id}.`,
        //     });
        // }
    }
}

// Retrieve all Tutorials from the database.
// export async function findAll(req: Request, res: Response) {
//     // const type = req.query.type;
//     // var condition = type ? { type: { [Op.iLike]: `%${type}%` } } : null;,
//     const server_state = "published";
//     var condition = { server_state: { [Op.eq]: `${server_state}` } };

//     Dataset.findAll({
//         where: condition,
//         include: [
//             "abstracts"
//         ],
//         order: ['server_date_published'],
//     })
//         .then((data) => {
//             res.send(data);
//         })
//         .catch((err) => {
//             res.status(500).send({
//                 message:
//                     err.message || "Some error occurred while retrieving datasets.",
//             });
//         });
// }

// export async function findOne(req: Request, res: Response) {
//     const publish_id = req.params.publish_id;

//     dbContext.Dataset.findOne({
//         where: { publish_id: publish_id },
//         include: [
//             "titles",
//             "abstracts",
//             {
//                 model: User,
//                 as: "user",
//             },
//             {
//                 model: Person,
//                 through: { where: { role: "author" } },
//                 as: "authors",
//             },
//             {
//                 model: Person,
//                 through: { where: { role: "contributor" } },
//                 as: "contributors",
//             },
//             "subjects",
//             "coverage",
//             "licenses",
//             "project",
//             "files",
//             "identifier"
//         ],
//         // order: ['server_date_published'],
//     })
//         .then((data) => {
//             if (data) {
//                 res.send(data);
//             } else {
//                 res.status(404).send({
//                     message: `Cannot find Dataset with publish_id=${publish_id}.`,
//                 });
//             }
//         })
//         .catch((err) => {
//             res.status(500).send({
//                 message: "Error retrieving Dataset with publish_id=" + publish_id,
//             });
//         });
// }

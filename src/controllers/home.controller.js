import sequelizeConnection from "../config/db.config";
import dbContext from "../models/init-models.js";
import Sequelize from "sequelize";
const Op = Sequelize.Op;
import { Person } from "../models/init-models.js";

export async function findYears(req, res, next) {
    const serverState = "published";
    // Use raw SQL queries to select all cars which belongs to the user
    try {
        const datasets = await sequelizeConnection.query(
            "SELECT distinct EXTRACT(YEAR FROM server_date_published) as published_date FROM gba.documents WHERE server_state = (:serverState)",
            {
                replacements: { serverState: serverState },
                type: sequelizeConnection.QueryTypes.SELECT,
                // attributes: [[sequelizeConnection.fn('DISTINCT', sequelizeConnection.col('published_date')), 'alias_name']],
            },
        );
        // Pluck the ids of the cars
        const years = datasets.map((dataset) => dataset.published_date);
        // check if the cars is returned
        // if (years.length > 0) {
        return res.status(200).json(years);
        // }
    } catch (error) {
        return next(error);
    }
}

export async function findDocumentsPerYear(req, res) {
    const year = req.params.year;
    const from = parseInt(year);
    const serverState = "published";

    const conditions = {
        [Op.and]: [
            {
                server_state: `${serverState}`,
            },
            {
                [Op.eq]: sequelizeConnection.where(
                    sequelizeConnection.fn("date_part", "year", sequelizeConnection.col("server_date_published")),
                    from,
                ),
                // [Op.eq]: sequelizeConnection.where(sequelizeConnection.fn('date', sequelizeConnection.col('server_date_published')), '>=', fromYear)
            },
        ],
    };

    dbContext.Dataset.findAll({
        attributes: [
            "publish_id",
            "server_date_published",
            [sequelizeConnection.fn("date_part", "year", sequelizeConnection.col("server_date_published")), "pub_year"],
        ],
        where: conditions,
        include: [
            "titles",
            {
                model: Person,
                through: { where: { role: "author" } },
                as: "authors",
            },
        ],
        order: ["server_date_published"],
    })
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving datasets.",
            });
        });
}

// function sendToElasticAndLogToConsole(sql, queryObject) {
//     // save the `sql` query in Elasticsearch
//     console.log(sql);
//     // use the queryObject if needed (e.g. for debugging)
// }

// const express = require('express');
// // const bodyParser = require('body-parser');
// // const cors = require('cors');
// // https://www.bezkoder.com/node-express-sequelize-postgresql/
// // Next, we'll want to instantiate the Express app:
// const app = express();
// app.use(express.static(__dirname + '/client'));

// https://github.com/ivan-shaban/nodemon-babel-preset-typescript/blob/master/README.md
// https://github.com/microsoft/TypeScript-Babel-Starter
import "core-js/stable";
import "regenerator-runtime/runtime";

import { App } from "./app";
const app: App = new App();
app.start();
// import express, { Express, Request, Response } from 'express';
// import bodyParser from 'body-parser';

// //Importing Routes
// import DatasetRoutes from './routes/dataset.routes.js';
// import HomeRoutes from './routes/home.routes.js';

// const app: Express = express();
//middlewares
// app.all('*', function(req, res, next) {
//     res.setHeader("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
//     res.header("Access-Control-Max-Age", "3600");
//     res.header("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, x-access-token");
//     next();
// });

// app.use(bodyParser.json({limit: '100mb'}));
// app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
// app.use(bodyParser.json({type: 'application/vnd.api+json'}));

// //routes
// app.use('/api/dataset', DatasetRoutes);
// app.use('/api/', HomeRoutes);

// Where we will keep books
// const books = [
//   {
//     isbn: "9781593275846",
//     title: "Eloquent JavaScript, Second Edition",
//     author: "Marijn Haverbeke",
//     publish_date: "2014-12-14",
//     publisher: "No Starch Press",
//     numOfPages: 472,
//   },
//   {
//     isbn: "9781449331818",
//     title: "Learning JavaScript Design Patterns",
//     author: "Addy Osmani",
//     publish_date: "2012-07-01",
//     publisher: "O'Reilly Media",
//     numOfPages: 254,
//   },
//   {
//     isbn: "9781449365035",
//     title: "Speaking JavaScript",
//     author: "Axel Rauschmayer",
//     publish_date: "2014-02-01",
//     publisher: "O'Reilly Media",
//     numOfPages: 460,
//   },
// ];

// load middlewar:
// app.use(cors());
// Configuring body parser middleware
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());
// app.use(express.json());

// const db = require("./models");
// db.sequelize.sync()
//   .then(() => {
//     console.log("Synced db.");
//   })
//   .catch((err) => {
//     console.log("Failed to sync db: " + err.message);
//   });

// app.post('/book', (req, res) => {
//     const book = req.body;

//     // Output the book to the console for debugging
//     console.log(book);
//     books.push(book);

//     res.send('Book is added to the database');
// });

// app.get('/books', async (req, res) => {
//     res.json(books);
//     //     const allDogs = await Dataset.findAll();
//     //   return res.status(200).json(allDogs);
// });

// app.get('/book/:isbn', (req, res) => {
//     // reading isbn from the URL
//     const isbn = req.params.isbn;

//     // searching books for the isbn
//     for (let book of books) {
//         if (book.isbn === isbn) {
//             res.json(book);
//             return;
//         }
//     }

//     // sending 404 when not found something is a good practice
//     res.status(404).send('Book not found');
// });

// app.post('/book/:isbn', (req, res) => {
//     // reading isbn from the URL
//     const isbn = req.params.isbn;
//     const newBook = req.body;

//     // remove item from the books array
//     for (let i = 0; i < books.length; i++) {
//         let book = books[i]

//         if (book.isbn === isbn) {
//             books[i] = newBook;
//         }
//     }

//     // sending 404 when not found something is a good practice
//     res.send('Book is edited');
// });

// // ow, we can create a simple GET endpoint right beneath the boilerplate.
// // We'd like to set it to be on the home page, so the URL for the endpoint is /:
// app.get('/', (request, response) => {
//     // response.send('Hello World, from express');
//     response.sendFile('/home/administrator/api/new-book.html');
// });

// app.get('/booklist', (request, response) => {
//     // response.send('Hello World, from express');
//     response.sendFile('/home/administrator/api/client/book-list.html');
// });

// // require("./routes/dataset.routes")(app);

// // set port, listen for requests
// const port = process.env.PORT || 3000;
// app.set('port', port);
// // At this point, let's start our clients:
// app.listen(app.get('port'), function () {
//     console.log('Express server listening on port ' + port);
// });

// export default app;

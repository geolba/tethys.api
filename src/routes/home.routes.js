import { Router } from "express";
// import "@babel/polyfill"
import { findYears, findDocumentsPerYear } from "../controllers/home.controller.js";

const router = Router();

// Retrieve all Tutorials
// router.get("/", findAll);
router.get("/years", findYears);
router.get("/sitelinks/:year", findDocumentsPerYear);

export default router;

// module.exports = app => {
//     const datasetController = require("../controllers/dataset.controller.js");

//     var router = require("express").Router();

//     // // Create a new Dataset
//     // router.post("/", datasetController.create);

//     // Retrieve all Tutorials
//     router.get("/", datasetController.findAll);

//     // // Retrieve all published Dataset
//     // router.get("/published", tutorials.findAllPublished);

//     // Retrieve a single Dataset with publish_id
//     router.get("/:publish_id", datasetController.findOne);

//     app.use('/api/dataset', router);
//   };

import { Router } from "express";
// import "@babel/polyfill"
import { findAll, findOne } from "../controllers/dataset.controller";

const router = new Router();

// Retrieve all Tutorials
router.get("/", findAll);
// router.get("/years/", findYears);
// Retrieve a single Dataset with publish_id
router.get("/:publish_id", findOne);
// router.post('/getUsers', getUsers);
// router.post('/getUser', getUser);
// router.post('/create', createUser);
// router.delete('/removeUser', deleteUser);
// router.put('/updateUser', updateUsers);

export default router;

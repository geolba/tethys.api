import { Server } from "@overnightjs/core";
import express from "express";
import bodyParser from "body-parser";
import HomeRoutes from "./routes/home.routes.js";
import { initDB } from "./config/db.config";
import { DatasetController } from "./controllers/dataset.controller";
import { OaiController } from "./controllers/oai.controller";
import { FileController } from "./controllers/file.controller";
import * as path from "path";
import HTTPException from "./exceptions/HttpException.js";

export class App extends Server {
    // private app;

    constructor() {
        super();
        // this.app = express();
        this.app.use("/assets", express.static(path.join(__dirname, "../assets")));      
        this.app.use("/favicon.ico", express.static('assets/favicon.ico'));
        this.applyMiddleWares();
        // init db and add routes
        this.boostrap();
    }

    public start(): void {
        const port = process.env.PORT || 3000;
        this.app.set("port", port);
        this.app.listen(port, () => {
            console.log("Server listening on port: " + port);
        });
    }

    private applyMiddleWares(): void {
        this.app.all("*", function (req, res, next) {
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
            res.header("Access-Control-Max-Age", "3600");
            res.header(
                "Access-Control-Allow-Headers",
                "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, x-access-token",
            );
            next();
        });

        // this.app.use(bodyParser.json());
        // for parsing application/json
        this.app.use(bodyParser.json({ limit: "100mb" }));
        // for parsing application/xwww-
        this.app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
        // this.app.use(forms.array());
        // for parsing multipart/form-data = uploads
        // this.app.use(upload.array());
    }

    private boostrap(): void {
        // Connect to db
        initDB()
            .then(() => {
                console.log("Connection has been established successfully.");

                // addroutes
                // this.app.use('/api/dataset', DatasetRoutes);
                super.addControllers([new DatasetController(), new OaiController(), new FileController()]);

                this.app.use("/api/", HomeRoutes);
                this.app.get("/", (request, response) => {
                    // response.send('Hello World, from express');
                    response.sendFile("/home/administrator/tethys.api/new-book.html");
                });

                // // Error handling middleware
                // this.app.use(errorHandler); // registration of handler
                // Error handling middleware
                this.app.use((error: HTTPException, req: express.Request, res: express.Response) => {
                    console.log("Oops !!, Error occured in req->res cycle ", error.message);
                    const status = error.status || 500;
                    res.status(status).json(error.message); // Send back Error to the Frontend
                });
            })
            .catch((err) => {
                console.error("Unable to connect to the database:", err);
            });
    }
}

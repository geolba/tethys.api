import Sequelize from "sequelize";
import sequelizeConnection from "../config/db.config";

const Subject = sequelizeConnection.define(
    "dataset_subjects",
    {
        type: {
            type: Sequelize.STRING,
            allowNull: false,
            length: 255,
        },
        value: {
            type: Sequelize.STRING,
            allowNull: false,
            length: 255,
        },
        external_key: {
            type: Sequelize.STRING,
            allowNull: true,
            length: 255,
        },
        language: Sequelize.STRING(3),
        created_at: Sequelize.DATE,
        updated_at: Sequelize.DATE,
    },
    {
        createdAt: "created_at",
        updatedAt: "updated_at",
        tableName: "dataset_subjects",
    },
);

export default Subject;

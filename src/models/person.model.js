import Sequelize from "sequelize";
import sequelizeConnection from "../config/db.config";

const Person = sequelizeConnection.define(
    "persons",
    {
        email: {
            type: Sequelize.STRING,
            allowNull: false,
            length: 255,
        },
        first_name: Sequelize.STRING(255),
        last_name: Sequelize.STRING(255),
    },
    {
        timestamps: false,
        tableName: "persons",
    },
);
export default Person;

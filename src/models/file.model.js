import Sequelize from "sequelize";
import sequelizeConnection from "../config/db.config";

const File = sequelizeConnection.define(
    "document_files",
    {
        path_name: {
            type: Sequelize.STRING,
            allowNull: false,
            length: 100,
        },
        label: Sequelize.STRING(100),
        comment: Sequelize.STRING(255),
        mime_type: Sequelize.STRING(255),
        language: Sequelize.STRING(3),
        file_size: {
            type: Sequelize.BIGINT,
            allowNull: false,
        },
        visible_in_frontdoor: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true,
        },
        visible_in_oai: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true,
        },
        sort_order: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        created_at: Sequelize.DATE,
        updated_at: Sequelize.DATE,
    },
    {
        createdAt: "created_at",
        updatedAt: "updated_at",
        tableName: "document_files",
    },
);

export default File;

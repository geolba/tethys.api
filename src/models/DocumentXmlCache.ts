// import {
//     Table,
//     PrimaryKey,
//     Column,
//     DataType,
//     Model,
//     UpdatedAt,
// } from "sequelize-typescript";

// @Table({
//     tableName: "document_xml_cache",
//     createdAt: false,
// })
// export default class DocumentXmlCache extends Model<DocumentXmlCache> {

//     @PrimaryKey
//     @Column({
//         type: DataType.INTEGER,
//         allowNull: false,
//     })
//     declare document_id: number;

//     @Column({
//         type: DataType.INTEGER,
//         allowNull: false,
//     })
//     declare xml_version: number;

//     @UpdatedAt
//     @Column({
//         field: 'server_date_modified',
//         type: DataType.DATE,
//         defaultValue: DataType.NOW
//     })
//     declare server_date_modified: Date;

//     @Column({
//         type: DataType.TEXT(),
//     })
//     declare xml_data: string;
// }

import { Op, Model, DataTypes, InferAttributes, InferCreationAttributes } from "sequelize";
import sequelizeConnection from "../config/db.config";
import { builder, create } from "xmlbuilder2";
import { XMLBuilder } from "xmlbuilder2/lib/interfaces";
// import { select, SelectedValue } from "xpath";
import dayjs from "dayjs";

class DocumentXmlCache extends Model<InferAttributes<DocumentXmlCache>, InferCreationAttributes<DocumentXmlCache>> {
    declare document_id: number;
    declare xml_version: number;

    // updatedAt can be undefined during creation
    // declare server_date_modified: CreationOptional<Date>;
    declare server_date_modified: string;

    declare xml_data: string;

    // // getters that are not attributes should be tagged using NonAttribute
    // // to remove them from the model's Attribute Typings.
    // get fullName(): NonAttribute<string | null> {
    //     return this.type;
    // }

    /**
     * Check if a dataset in a specific xml version is already cached or not.
     *
     * @param mixed datasetId
     * @param mixed serverDateModified
     * @returns {bool} Returns true on cached hit else false.
     */
    public static async hasValidEntry(datasetId: number, datasetServerDateModified: Date): Promise<boolean> {
        const condition = {
            [Op.and]: [
                {
                    document_id: { [Op.eq]: datasetId },
                    server_date_modified: { [Op.eq]: dayjs(datasetServerDateModified).format("YYYY-MM-DD HH:mm:ss") },
                },
            ],
        };
        //   $select = DB::table('document_xml_cache');
        //   $select->where('document_id', '=', $datasetId)
        //   ->where('server_date_modified', '=', $serverDateModified);

        const model = await this.findOne({
            where: condition,
            order: [["server_date_modified", "asc"]],
        });
        if (model) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get dom document of 'xml_data' string
     *
     * @returns {XMLBuilder}
     */
    public getDomDocument(): XMLBuilder {
        // const dom = xmlbuilder.create({ version: "1.0", encoding: "UTF-8", standalone: true });
        let dom: XMLBuilder = create({ version: "1.0", encoding: "UTF-8", standalone: true }, this.xml_data);
        // return dom.first();

        const rdrDataset = dom.find(
            (n) => {
                const test = n.node.nodeName == "Rdr_Dataset";
                return test;
            },
            false,
            true,
        )?.node;

        if (rdrDataset == undefined) {
            return dom.first();
        } else {
            dom = builder({ version: "1.0", encoding: "UTF-8", standalone: true }, rdrDataset);
            return dom;
        }
        // const doc2 = create().doc();
        // rdrDataset && doc2.import(rdrDataset);
        // return doc2.first();
        // rdrDataset && (dom = builder({ version: '1.0', encoding: 'UTF-8', standalone : true }, rdrDataset));

        // let domNode: Node = dom.node;
        // // run the xpath query
        // const result: Array<SelectedValue> = select("//Rdr_Dataset", domNode);
        // if (result && result.length && result.length > 0) {
        //     // convert the DOM node to a builder object

        //     const recordNode = builder(result.at(0));
        //     const doc2 = create().doc();
        //     // import into result document
        //     doc2.import(recordNode);
        //     // console.log(doc2.end({ prettyPrint: true }));
        //     return doc2;
        // }
        // return dom.first();
    }
}

DocumentXmlCache.init(
    {
        document_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        xml_version: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        server_date_modified: DataTypes.STRING(50),
        xml_data: {
            type: DataTypes.TEXT,
        },
    },
    {
        createdAt: false,
        updatedAt: false, //"server_date_modified",
        sequelize: sequelizeConnection,
        tableName: "document_xml_cache",
    },
);

export default DocumentXmlCache;

import { Model, DataTypes, InferAttributes, InferCreationAttributes, CreationOptional } from "sequelize";
import sequelizeConnection from "../config/db.config";

class Identifier extends Model<InferAttributes<Identifier>, InferCreationAttributes<Identifier>> {
    // id can be undefined during creation when using `autoIncrement`
    declare id: CreationOptional<number>;
    declare value: string; // not nullable fields
    declare type: string;
    declare status: string | null;

    // createdAt can be undefined during creation
    declare created_at: CreationOptional<Date>;
    // updatedAt can be undefined during creation
    declare updated_at: CreationOptional<Date>;
}

Identifier.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        value: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        type: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        status: DataTypes.STRING(255),
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE,
    },
    {
        createdAt: "created_at",
        updatedAt: "updated_at",
        tableName: "dataset_identifiers",
        sequelize: sequelizeConnection,
    },
);

export default Identifier;

// import Sequelize from 'sequelize'
import sequelizeConnection from "../config/db.config";
import Dataset from "./Dataset";
import Title from "./title.model.js";
import Abstract from "./abstract.model.js";
import Coverage from "./coverage.model.js";
import License from "./license.model.js";
import User from "./user.model.js";
// import Person from "./person.model.js";
import Person from "./Person";
import { Sequelize } from "sequelize";
import Subject from "./subject.model.js";
import Reference from "./Reference";
import Project from "./Project";
// import File from "./file.model.js";
import File from "./File";
import HashValue from "./HashValue";
import Identifier from "./Identifier";
import DocumentXmlCache from "./DocumentXmlCache";
import CollectionRole from "./CollectionRole";
import Collection from "./Collection";

const dbContext = initModels();

export {
    Dataset,
    Title,
    Abstract,
    User,
    Person,
    Subject,
    Coverage,
    License,
    Reference,
    Project,
    Identifier,
    DocumentXmlCache,
    File,
    HashValue,
    Collection,
    CollectionRole,
};
export default dbContext;

export function initModels() {
    // title identifier
    Dataset.hasOne(Identifier, {
        as: "identifier",
        foreignKey: "dataset_id",
    });
    Identifier.belongsTo(Dataset, {
        foreignKey: "dataset_id",
        as: "dataset",
    });

    // title relations
    Dataset.hasMany(Title, {
        as: "titles",
        foreignKey: "document_id",
    });
    Title.belongsTo(Dataset, {
        foreignKey: "document_id",
        as: "dataset",
    });

    // abstract relations
    Dataset.hasMany(Abstract, {
        as: "abstracts",
        foreignKey: "document_id",
    });
    Abstract.belongsTo(Dataset, {
        foreignKey: "document_id",
        as: "dataset",
    });

    //user relations
    User.hasMany(Dataset, {
        foreignKey: "account_id",
        as: "datasets",
    });
    Dataset.belongsTo(User, {
        foreignKey: "account_id",
        as: "user",
    });

    // authors and contributors
    const DocumentPersons = sequelizeConnection.define(
        "link_documents_persons",
        {
            person_id: { type: Sequelize.INTEGER, primaryKey: true },
            document_id: { type: Sequelize.INTEGER, primaryKey: true },
            // role: { type: Sequelize.STRING(255), defaultValue: "other", primaryKey: true},
            role: {
                type: Sequelize.ENUM("author", "contributor", "other"),
                // scopes: {
                //   author: ["author"],
                //   contributor: ["contributor"],
                // },
            },
            sort_order: Sequelize.INTEGER,
            allow_email_contact: Sequelize.BOOLEAN,
            contributor_type: Sequelize.STRING(255),
        },
        {
            // schema: 'public',
            tableName: "link_documents_persons",
            timestamps: false,
        },
    );

    // relation authors
    Dataset.belongsToMany(Person, {
        through: {
            model: DocumentPersons,
            // scope: { role: "author" },
        },
        as: "authors",
        foreignKey: "document_id",
    });
    Person.belongsToMany(Dataset, {
        through: {
            model: DocumentPersons,
        },
        foreignKey: "person_id",
        as: "a_datasets",
    });

    // relation contributors
    Dataset.belongsToMany(Person, {
        through: {
            model: DocumentPersons,
        },
        as: "contributors",
        foreignKey: "document_id",
    });
    Person.belongsToMany(Dataset, {
        through: {
            model: DocumentPersons,
        },
        foreignKey: "person_id",
        as: "c_datasets",
    });

    //subjects
    const DatasetSubject = sequelizeConnection.define(
        "link_dataset_subjects",
        {},
        {
            tableName: "link_dataset_subjects",
            timestamps: false,
        },
    );
    Dataset.belongsToMany(Subject, {
        through: DatasetSubject,
        as: "subjects",
        foreignKey: "document_id",
    });
    Subject.belongsToMany(Dataset, {
        through: DatasetSubject,
        foreignKey: "subject_id",
        as: "datasets",
    });

    // coverage
    Dataset.hasOne(Coverage, {
        as: "coverage",
        foreignKey: "dataset_id",
    });
    Coverage.belongsTo(Dataset, {
        foreignKey: "dataset_id",
        as: "dataset",
    });

    // xmlCache
    Dataset.hasOne(DocumentXmlCache, {
        as: "xmlCache",
        foreignKey: "document_id",
    });
    // Coverage.belongsTo(Dataset, {
    //     foreignKey: "dataset_id",
    //     as: "dataset",
    // });

    //licences
    const DatasetLicense = sequelizeConnection.define(
        "link_documents_licences",
        {},
        {
            tableName: "link_documents_licences",
            timestamps: false,
        },
    );
    Dataset.belongsToMany(License, {
        through: DatasetLicense,
        as: "licenses",
        foreignKey: "document_id",
    });
    License.belongsToMany(Dataset, {
        through: DatasetLicense,
        foreignKey: "licence_id",
        as: "datasets",
    });

    //project references
    Dataset.hasMany(Reference, {
        foreignKey: "document_id",
        as: "references",
    });
    Reference.belongsTo(Dataset, {
        foreignKey: "document_id",
        as: "dataset",
    });
    Dataset.hasMany(Reference, {
        foreignKey: "related_document_id",
        as: "referenced_by",
    });
    Reference.belongsTo(Dataset, {
        foreignKey: "related_document_id",
        as: "new_dataset",
        include: "identifier"
    });

    //project relations
    Project.hasMany(Dataset, {
        foreignKey: "project_id",
        as: "datasets",
    });
    Dataset.belongsTo(Project, {
        foreignKey: "project_id",
        as: "project",
    });

    //file relations
    // title relations
    Dataset.hasMany(File, {
        as: "files",
        foreignKey: "document_id",
    });
    File.belongsTo(Dataset, {
        foreignKey: "document_id",
        as: "dataset",
    });

    File.hasMany(HashValue, {
        as: "hashvalues",
        foreignKey: "file_id",
    });
    HashValue.belongsTo(File, {
        foreignKey: "file_id",
        as: "file",
    });

    // collection an collectionRole relations
    CollectionRole.hasMany(Collection, {
        as: "collections",
        foreignKey: "role_id",
    });
    Collection.belongsTo(CollectionRole, {
        foreignKey: "role_id",
        as: "collectionRole",
    });

    // dataset and collections
    //licences
    const DatasetCollection = sequelizeConnection.define(
        "link_documents_collections",
        {},
        {
            tableName: "link_documents_collections",
            timestamps: false,
        },
    );
    Dataset.belongsToMany(Collection, {
        through: DatasetCollection,
        as: "collections",
        foreignKey: "document_id",
    });
    Collection.belongsToMany(Dataset, {
        through: DatasetCollection,
        foreignKey: "collection_id",
        as: "datasets",
    });

    return {
        Dataset: Dataset,
        Title: Title,
        Coverage: Coverage,
        Subject: Subject,
        License: License,
        DocumentPersons: DocumentPersons,
        File: File,
        Reference: Reference
    };
}

import Sequelize from "sequelize";
import sequelizeConnection from "../config/db.config";

// module.exports = (sequelize, DataTypes) => {

const Coverage = sequelizeConnection.define(
    "coverage",
    {
        elevation_min: Sequelize.INTEGER,
        elevation_max: Sequelize.INTEGER,
        elevation_absolut: Sequelize.INTEGER,
        depth_min: Sequelize.INTEGER,
        depth_max: Sequelize.INTEGER,
        depth_absolut: Sequelize.INTEGER,
        time_min: Sequelize.INTEGER,
        time_max: Sequelize.INTEGER,
        time_absolut: Sequelize.INTEGER,
        x_min: Sequelize.DOUBLE,
        x_max: Sequelize.DOUBLE,
        y_min: Sequelize.DOUBLE,
        y_max: Sequelize.DOUBLE,
        created_at: Sequelize.DATE,
        updated_at: Sequelize.DATE,
    },
    {
        createdAt: "created_at",
        updatedAt: "updated_at",
        // schema: 'public',
        tableName: "coverage",
    },
);

export default Coverage;
// };

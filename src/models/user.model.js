import Sequelize from "sequelize";
import sequelizeConnection from "../config/db.config";

const User = sequelizeConnection.define(
    "accounts",
    {
        login: {
            type: Sequelize.STRING,
            allowNull: false,
            length: 20,
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false,
            length: 60,
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false,
            length: 255,
        },
        first_name: Sequelize.STRING(255),
        last_name: Sequelize.STRING(255),
        remember_token: Sequelize.STRING(255),
        created_at: Sequelize.DATE,
        updated_at: Sequelize.DATE,
    },
    {
        defaultScope: {
            attributes: {
                exclude: ["password", "remember_token"],
            },
        },
        createdAt: "created_at",
        updatedAt: "updated_at",
        tableName: "accounts",
    },
);
export default User;

import { Model, DataTypes, InferAttributes, InferCreationAttributes, CreationOptional } from "sequelize";
import sequelizeConnection from "../config/db.config";

class Person extends Model<InferAttributes<Person>, InferCreationAttributes<Person>> {
    // id can be undefined during creation when using `autoIncrement`
    declare id: CreationOptional<number>;
    declare academic_title: string | null; // for nullable fields
    declare email: string; // not nullable fields
    declare first_name: string | null; // for nullable fields
    declare last_name: string;
    declare place_of_birth: string | null;
    declare identifier_orcid: string | null;
    declare identifier_gnd: string | null;
    declare identifier_misc: string | null;
    declare status: boolean | null;
    declare name_type: string | null; // for nullable fields

    // getters that are not attributes should be tagged using NonAttribute
    // to remove them from the model's Attribute Typings.
    get full_name(): string {
        return this.first_name + " " + this.last_name;
    }
}

Person.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        academic_title: DataTypes.STRING(255),
        email: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        first_name: DataTypes.STRING(255),
        last_name: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        place_of_birth: DataTypes.STRING(255),
        identifier_orcid: DataTypes.STRING(50),
        identifier_gnd: DataTypes.STRING(50),
        identifier_misc: DataTypes.STRING(50),
        status: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
        },
        name_type: DataTypes.STRING(255),
        full_name: {
            type: new DataTypes.VIRTUAL(DataTypes.STRING(255), ["first_name", "last_name"]),
            get(): string {
                return this.first_name + " " + this.last_name;
            },
        },
    },
    {
        timestamps: false,
        tableName: "persons",
        sequelize: sequelizeConnection,
    },
);

export default Person;

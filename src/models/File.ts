import { Model, DataTypes, InferAttributes, InferCreationAttributes, CreationOptional } from "sequelize";
import sequelizeConnection from "../config/db.config";

class File extends Model<InferAttributes<File>, InferCreationAttributes<File>> {
    // id can be undefined during creation when using `autoIncrement`
    declare id: CreationOptional<number>;
    declare path_name: string; // not nullable fields
    declare label: string | null;
    declare comment: string | null;
    declare mime_type: string;
    declare language: string | null;
    declare file_size: bigint;
    declare visible_in_frontdoor: boolean;
    declare visible_in_oai: boolean;
    declare sort_order: number;

    // createdAt can be undefined during creation
    declare created_at: CreationOptional<Date>;
    // updatedAt can be undefined during creation
    declare updated_at: CreationOptional<Date>;
}

File.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        path_name: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        label: DataTypes.STRING(100),
        comment: DataTypes.STRING(255),
        mime_type: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        language: DataTypes.STRING(3),
        file_size: {
            type: DataTypes.BIGINT,
            allowNull: false,
        },
        visible_in_frontdoor: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true,
        },
        visible_in_oai: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true,
        },
        sort_order: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },

        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE,
    },
    {
        createdAt: "created_at",
        updatedAt: "updated_at",
        tableName: "document_files",
        sequelize: sequelizeConnection,
    },
);

export default File;

import Sequelize from "sequelize";
import sequelizeConnection from "../config/db.config";

const Abstract = sequelizeConnection.define(
    "dataset_abstracts",
    {
        type: {
            type: Sequelize.STRING,
            allowNull: false,
            length: 255,
        },
        value: {
            type: Sequelize.TEXT,
            allowNull: false,
        },
        language: {
            type: Sequelize.STRING,
            allowNull: false,
            length: 3,
        },
    },
    {
        timestamps: false,
        tableName: "dataset_abstracts",
    },
);
// Abstract.belongsTo(Dataset, {
//       foreignKey: "document_id",
//       as: "dataset",
//     });

export default Abstract;

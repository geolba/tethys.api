import Sequelize from "sequelize";
import sequelizeConnection from "../config/db.config";

// module.exports = (sequelize, Sequelize) => {
const Title = sequelizeConnection.define(
    "dataset_titles",
    {
        type: Sequelize.STRING,
        value: Sequelize.STRING,
        language: Sequelize.STRING,
    },
    {
        // schema: 'public',
        timestamps: false,
        tableName: "dataset_titles",
    },
);
// Title.belongsTo(Dataset, {
//       foreignKey: "document_id",
//       as: "dataset",
//     });

export default Title;

//   return Title;
// };

// import {
//     Table,
//     Column,
//     DataType,
//     IsEmail,
//     HasMany,
//     Model,
//     CreatedAt, UpdatedAt,
//     BelongsToMany,
//     BelongsTo
// } from "sequelize-typescript";
// import { Abstract } from "./Abstract";

// @Table({
//     tableName: "documents"
// })
// export class Dataset extends Model<Dataset> {

//     @Column({
//         // field: 'type',
//         type: DataType.STRING(255),
//         allowNull: false,
//     })
//     type!: string;

//     @CreatedAt
//     @Column({
//         field: 'created_at',
//         type: DataType.DATE,
//         defaultValue: DataType.NOW
//     })
//     created_at!: Date;

//     @UpdatedAt
//     @Column({
//         field: 'server_date_modified',
//         type: DataType.DATE,
//         defaultValue: DataType.NOW
//     })
//     server_date_modified!: Date;

//     @Column({
//         field: 'server_state',
//         type: DataType.STRING(),
//     })
//     serverState!: string;

//     @HasMany(() => Abstract, "document_id")
//     public readonly abstracts?: Abstract[];
// }

import { Op, Model, DataTypes, InferAttributes, InferCreationAttributes, CreationOptional, NonAttribute, Association, HasManyGetAssociationsMixin } from "sequelize";
import sequelizeConnection from "../config/db.config";
import DocumentXmlCache from "./DocumentXmlCache";
import Collection from "./Collection";
import Reference from "./Reference";
import File from "./File";

class Dataset extends Model<InferAttributes<Dataset>, InferCreationAttributes<Dataset>> {
    // id can be undefined during creation when using `autoIncrement`
    declare id: CreationOptional<number>;
    declare contributing_corporation: string | null; // for nullable fields
    declare creating_corporation: string; // not nullable fields
    declare publisher_name: string | null;
    declare embargo_date: Date | null;
    declare publish_id: number | null;

    declare type: string | null;
    declare language: string | null; // for nullable fields
    declare server_state: string | null;
    declare server_date_published: Date | null;

    // createdAt can be undefined during creation
    declare created_at: CreationOptional<Date>;
    // updatedAt can be undefined during creation
    declare server_date_modified: CreationOptional<Date>;

    // You can also pre-declare possible inclusions, these will only be populated if you
    // actively include a relation.
    declare xmlCache?: NonAttribute<DocumentXmlCache>; // Note this is optional since it's only populated when explicitly requested in code

    declare collections?: NonAttribute<Collection[]>;
    declare references?: NonAttribute<Reference[]>;
    declare files?: NonAttribute<File[]>;
    // declare static associations: {

    // };
    declare getFiles: HasManyGetAssociationsMixin<File>;

    // getters that are not attributes should be tagged using NonAttribute
    // to remove them from the model's Attribute Typings.
    get fullName(): NonAttribute<string | null> {
        return this.type;
    }

    declare static associations: {
        xmlCache: Association<Dataset, DocumentXmlCache>;
        collections: Association<Dataset, Collection>;
        references: Association<Dataset, Reference>;
        files: Association<Dataset, File>;
    };

    public static async earliestPublicationDate(): Promise<Dataset | null> {
        const server_state = "published";
        const condition = {
            [Op.and]: [
                {
                    server_state: { [Op.eq]: server_state },
                },
            ],
        };

        const model = await this.findOne({
            attributes: ["server_date_published"],
            where: condition,
            order: [["server_date_published", "asc"]],
        });
        if (model) {
            return model;
        } else {
            return null;
        }
    }
}

Dataset.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        contributing_corporation: { type: DataTypes.STRING(255) },
        creating_corporation: { type: DataTypes.STRING(255), allowNull: false },
        publisher_name: DataTypes.STRING(255),
        embargo_date: DataTypes.DATE,
        publish_id: DataTypes.INTEGER,

        type: {
            type: DataTypes.STRING,
        },
        language: DataTypes.STRING,
        server_state: {
            type: DataTypes.STRING,
        },
        server_date_published: DataTypes.DATE,

        // project_id: DataTypes.INTEGER,
        // embargo_date: DataTypes.DATE,
        // belongs_to_bibliography: DataTypes.BOOLEAN,
        // editor_id: DataTypes.INTEGER,
        // preferred_reviewer: DataTypes.STRING,
        // preferred_reviewer_email: DataTypes.STRING,
        // reviewer_id: DataTypes.INTEGER,
        // reject_reviewer_note: DataTypes.STRING,
        // reject_editor_note: DataTypes.STRING,
        // reviewer_note_visible: DataTypes.BOOLEAN,

        created_at: DataTypes.DATE,
        server_date_modified: DataTypes.DATE,
    },
    {
        createdAt: "created_at",
        updatedAt: "server_date_modified",
        sequelize: sequelizeConnection,
        tableName: "documents",
    },
);

export default Dataset;

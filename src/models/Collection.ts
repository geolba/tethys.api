// import Sequelize from "sequelize";
import {
    Model,
    DataTypes,
    InferAttributes,
    InferCreationAttributes,
    CreationOptional,
    NonAttribute,
    Association,
    BelongsToGetAssociationMixin,
} from "sequelize";
import sequelizeConnection from "../config/db.config";
import CollectionRole from "./CollectionRole";
import Dataset from "./Dataset";

class Collection extends Model<InferAttributes<Collection>, InferCreationAttributes<Collection>> {
    // id can be undefined during creation when using `autoIncrement`
    declare id: CreationOptional<number>;
    declare role_id: number | null;
    declare number: string | null; // nullable fields
    declare name: string; // not nullable fields
    declare oai_subset: string | null; // nullable fields
    declare parent_id: number | null;
    declare visible: boolean;
    declare visible_publish: boolean;

    // https://sequelize.org/docs/v6/other-topics/typescript/
    // Since TS cannot determine model association at compile time
    // we have to declare them here purely virtually
    // these will not exist until `Model.init` was called.
    declare getCollectionRole: BelongsToGetAssociationMixin<CollectionRole>;

    // You can also pre-declare possible inclusions, these will only be populated if you
    // actively include a relation.belongsTo one collectionRole
    declare collectionRole?: NonAttribute<CollectionRole>;
    declare datasets?: NonAttribute<Dataset>;

    declare static associations: {
        collectionRole: Association<Collection, CollectionRole>;
        datasets: Association<Collection, Dataset>;
    };

    // getters that are not attributes should be tagged using NonAttribute
    // to remove them from the model's Attribute Typings.
    get fullName(): NonAttribute<string> {
        return this.name + ":" + this.number;
    }
}

Collection.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        role_id: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        number: {
            type: DataTypes.STRING(255),
            allowNull: true,
        },
        name: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        oai_subset: {
            type: DataTypes.STRING(255),
            allowNull: true,
        },
        parent_id: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        visible: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
        },
        visible_publish: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
        },
    },
    {
        timestamps: false,
        sequelize: sequelizeConnection,
        tableName: "collections",
    },
);
export default Collection;

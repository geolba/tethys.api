import Sequelize from "sequelize";
import sequelizeConnection from "../config/db.config";

// module.exports = (sequelize, DataTypes) => {

const License = sequelizeConnection.define(
    "coverage",
    {
        active: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true,
        },
        comment_internal: Sequelize.STRING,
        desc_markup: Sequelize.STRING,
        desc_text: Sequelize.STRING,
        language: Sequelize.STRING,
        link_licence: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        link_logo: Sequelize.STRING,
        link_sign: Sequelize.STRING,
        mime_type: Sequelize.STRING,
        name_long: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        pod_allowed: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        sort_order: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
    },
    {
        timestamps: false,
        tableName: "document_licences",
    },
);

export default License;
// };

// import Sequelize from "sequelize";
import { Model, DataTypes, InferAttributes, InferCreationAttributes, CreationOptional } from "sequelize";
import sequelizeConnection from "../config/db.config";

class Project extends Model<InferAttributes<Project>, InferCreationAttributes<Project>> {
    // id can be undefined during creation when using `autoIncrement`
    declare id: CreationOptional<number>;
    declare label: string; // not nullable fields
    declare name: string;

    // createdAt can be undefined during creation
    declare created_at: CreationOptional<Date>;
    // updatedAt can be undefined during creation
    declare updated_at: CreationOptional<Date>;
}

Project.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        label: {
            type: DataTypes.STRING(50),
            allowNull: false,
        },
        name: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE,
    },
    {
        createdAt: "created_at",
        updatedAt: "updated_at",
        sequelize: sequelizeConnection,
        tableName: "projects",
    },
);
export default Project;

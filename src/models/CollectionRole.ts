// import Sequelize from "sequelize";
import { Model, DataTypes, InferAttributes, InferCreationAttributes, CreationOptional, Association, NonAttribute } from "sequelize";
import sequelizeConnection from "../config/db.config";
import Collection from "./Collection";

class CollectionRole extends Model<InferAttributes<CollectionRole>, InferCreationAttributes<CollectionRole>> {
    // id can be undefined during creation when using `autoIncrement`
    declare id: CreationOptional<number>;
    declare name: string; // not nullable fields
    declare oai_name: string; // not nullable fields
    declare position: number;
    declare visible: boolean;
    declare visible_frontdoor: boolean;
    declare visible_oai: boolean;

    // You can also pre-declare possible inclusions, these will only be populated if you
    // actively include a relation.
    declare collections?: NonAttribute<Collection>;
    declare static associations: {
        collections: Association<CollectionRole, Collection>;
    };
}

CollectionRole.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },

        name: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        oai_name: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        position: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        visible: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
        },
        visible_frontdoor: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
        },
        visible_oai: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
        },
    },
    {
        timestamps: false,
        sequelize: sequelizeConnection,
        tableName: "collections_roles",
    },
);
export default CollectionRole;

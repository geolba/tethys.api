import Sequelize from "sequelize";
import sequelizeConnection from "../config/db.config";

const Dataset = sequelizeConnection.define(
    "documents",
    {
        type: {
            type: Sequelize.STRING,
        },
        language: Sequelize.STRING,
        server_state: {
            type: Sequelize.STRING,
        },
        server_date_published: Sequelize.DATE,
        publisher_name: Sequelize.STRING,
        publish_id: Sequelize.INTEGER,
        creating_corporation: Sequelize.STRING,
        project_id: Sequelize.INTEGER,
        embargo_date: Sequelize.DATE,
        belongs_to_bibliography: Sequelize.BOOLEAN,
        editor_id: Sequelize.INTEGER,
        preferred_reviewer: Sequelize.STRING,
        preferred_reviewer_email: Sequelize.STRING,
        reviewer_id: Sequelize.INTEGER,
        reject_reviewer_note: Sequelize.STRING,
        reject_editor_note: Sequelize.STRING,
        reviewer_note_visible: Sequelize.BOOLEAN,

        created_at: Sequelize.DATE,
        server_date_modified: Sequelize.DATE,
    },
    {
        createdAt: "created_at",
        updatedAt: "server_date_modified",
        // schema: 'public',
        tableName: "documents",
    },
);
// // relations
// Dataset.hasMany(Title, {
//     as: "titles",
//     foreignKey: "document_id"
//   });
//   Title.belongsTo(Dataset, {
//     foreignKey: "document_id",
//     as: "dataset",
//   });

export default Dataset;

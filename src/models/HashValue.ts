import { Model, DataTypes, InferAttributes, InferCreationAttributes, CreationOptional } from "sequelize";
import sequelizeConnection from "../config/db.config";

class HashValue extends Model<InferAttributes<HashValue>, InferCreationAttributes<HashValue>> {
    // id can be undefined during creation when using `autoIncrement`
    declare file_id: number;
   
    declare type: string;
    declare value: string;   

    // // createdAt can be undefined during creation
    // declare created_at: CreationOptional<Date>;
    // // updatedAt can be undefined during creation
    // declare updated_at: CreationOptional<Date>;
}

HashValue.init(
    {
        file_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        type: {
            type: DataTypes.STRING(50),
            allowNull: false,
            primaryKey: true,
        },
        value: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
       

        // created_at: DataTypes.DATE,
        // updated_at: DataTypes.DATE,
    },
    {
        // createdAt: "created_at",
        // updatedAt: "updated_at",
        timestamps: false,
        tableName: "file_hashvalues",
        sequelize: sequelizeConnection,
    },
);

export default HashValue;

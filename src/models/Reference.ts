// import Sequelize from "sequelize";
import {
    Model,
    DataTypes,
    InferAttributes,
    InferCreationAttributes,
    CreationOptional,
    NonAttribute,
    Association,
    BelongsToGetAssociationMixin,
} from "sequelize";
import sequelizeConnection from "../config/db.config";
import Dataset from "./Dataset";

class Reference extends Model<InferAttributes<Reference>, InferCreationAttributes<Reference>> {
    // id can be undefined during creation when using `autoIncrement`
    declare id: CreationOptional<number>;
    declare document_id: number;
    // declare role_id: number | null;
    declare type: string; // nullable fields
    declare relation: string; // not nullable fields
    declare value: string; // nullable fields
    declare label: string; // nullable fields
    declare created_at: CreationOptional<Date>;
    // updatedAt can be undefined during creation
    declare updated_at: CreationOptional<Date>;
    declare related_document_id?: number;

    // https://sequelize.org/docs/v6/other-topics/typescript/
    // Since TS cannot determine model association at compile time
    // we have to declare them here purely virtually
    // these will not exist until `Model.init` was called.
    declare getDataset: BelongsToGetAssociationMixin<Dataset>;

    // You can also pre-declare possible inclusions, these will only be populated if you
    // actively include a relation.belongsTo one collectionRole
    declare dataset?: NonAttribute<Dataset>;
    // declare datasets?: NonAttribute<Dataset>;

    declare static associations: {
        dataset: Association<Reference, Dataset>;
        // datasets: Association<Collection, Dataset>;
    };

    // getters that are not attributes should be tagged using NonAttribute
    // to remove them from the model's Attribute Typings.
    get fullName(): NonAttribute<string> {
        return this.value + ":" + this.label;
    }
}

Reference.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        document_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        // role_id: {
        //     type: DataTypes.INTEGER,
        //     allowNull: true,
        // },
        type: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        relation: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        value: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        label: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE,
        // visible_publish: {
        //     type: DataTypes.BOOLEAN,
        //     defaultValue: true,
        // },
    },
    {
        // timestamps: false,
        createdAt: "created_at",
        updatedAt: "updated_at",
        sequelize: sequelizeConnection,
        tableName: "document_references",
    },
);
export default Reference;

import { StatusCodes } from "http-status-codes";
import HTTPException from "./HttpException";

class BadRequestException extends HTTPException {
    constructor(message?: string) {
        super(StatusCodes.BAD_REQUEST, message || "bad Request");
        this.stack = "";
    }
}

export default BadRequestException;

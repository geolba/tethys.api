import { Request, Response } from "express";
// import { ErrorCode } from './error-code';
// import { ErrorException } from './error-exception';
// import { ErrorModel } from './error-model';
import HTTPException from "./HttpException";
import { StatusCodes } from "http-status-codes";
import { OaiModelException } from "./OaiModelException";

export const errorHandler = (err: HTTPException | OaiModelException, req: Request, res: Response) => {
    console.log("Error handling middleware called.");
    console.log("Path:", req.path);
    console.error("Error occured:", err);
    if (err instanceof HTTPException) {
        console.log("Http Error is known.");
        res.status(err.status).send(err);
    } else if (err instanceof OaiModelException) {
        console.log("Oai-Error is known.");
        res.status(err.status).send(err);
    } else {
        // For unhandled errors.
        res.status(500).send({ code: StatusCodes.INTERNAL_SERVER_ERROR, status: 500 });
    }
};

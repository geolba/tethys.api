import { StatusCodes } from "http-status-codes";
import HTTPException from "./HttpException";

class PageNotFoundException extends HTTPException {
    constructor(message?: string) {
        super(StatusCodes.NOT_FOUND, message || "Page not found");
        this.stack = "";
    }
}

export default PageNotFoundException;

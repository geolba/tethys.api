import DocumentXmlCache from "../models/DocumentXmlCache";
// import { XMLDocument } from "xmlbuilder";
import { XMLBuilder } from "xmlbuilder2/lib/interfaces";
import Dataset from "../models/Dataset";
import Logger from "jet-logger";
// import { create } from "xmlbuilder2";
// import dayjs from "dayjs";

/**
 * This is the description of the interface
 *
 * @interface Conf
 * @member {Model} model holds the current dataset model
 * @member {XMLBuilder} dom holds the current DOM representation
 */
export interface Conf {
    /**
     * Holds the current model either directly set or deserialized from XML.
     */
    model: Dataset;

    /**
     * Holds the current DOM representation.
     */
    dom?: XMLBuilder;

    /**
     * List of fields to skip on serialization.
     */
    excludeFields: Array<string>;

    /**
     * True, if empty fields get excluded from serialization.
     */
    excludeEmpty: boolean;

    /**
     * Base URI for xlink:ref elements
     */
    baseUri: string;
}

export default class XmlModel {
    // private config: { [key: string]: any } = {};
    private config: Conf; // = { excludeEmpty: false, baseUri: "" };
    // private strategy = null;
    private cache: DocumentXmlCache;
    private _caching = false;

    constructor(dataset: Dataset) {
        // $this->strategy = new Strategy();// Opus_Model_Xml_Version1;
        // $this->config = new Conf();
        // $this->strategy->setup($this->config);

        // this.strategy = new Strategy();
        this.config = {
            excludeEmpty: false,
            baseUri: "",
            excludeFields: [],
            model: dataset,
        };
    }

    /**
     * Set the Model for XML generation.
     *
     * @param \App\Models\Dataset model Model to serialize.
     *
     */
    set setModel(model: Dataset) {
        this.config.model = model;
        // return this;
    }

    /**
     * Define that empty fields (value===null) shall be excluded.
     *
     */
    public excludeEmptyFields(): void {
        this.config.excludeEmpty = true;
        //   return this;
    }

    /**
     * Return cache table.
     *
     * @returns {DocumentXmlCache}
     */
    get getXmlCache(): DocumentXmlCache {
        return this.cache;
    }

    /**
     * Set a new XML version with current configuration up.
     *
     * @param { DocumentXmlCache } cache table
     */
    set setXmlCache(cache: DocumentXmlCache) {
        this.cache = cache;
    }

    get caching(): boolean {
        return this._caching;
    }
    set caching(caching: boolean) {
        this._caching = caching;
    }

    public async getDomDocument() {
        // const dataset = this.config.model;

        const domDocument: XMLBuilder | null = await this.getDomDocumentFromXmlCache();
        if (domDocument) {
            return domDocument;
        } else {
            //create domDocument during runtime
            // domDocument = $this->strategy->getDomDocument();
            // domDocument = create({ version: "1.0", encoding: "UTF-8", standalone: true }, "<root></root>");
            return null;
        }

        // //if caching isn't wanted return only dom Document
        // if (this._caching != true) {
        //     return domDocument;
        //     //otherwise caching is desired:
        //     // save xml cache to db and return domDocument
        // } else {
        //     // save new DocumentXmlCache
        //     if (!this.cache) {
        //         this.cache = new DocumentXmlCache();
        //         this.cache.document_id = dataset.id;
        //     }
        //     // if (!this.cache.document_id) {
        //     //     this.cache.document_id = dataset.id;
        //     // }
        //     this.cache.xml_version = 1; // (int)$this->strategy->getVersion();
        //     this.cache.server_date_modified = dayjs(dataset.server_date_modified).format("YYYY-MM-DD HH:mm:ss");
        //     this.cache.xml_data = domDocument.end();

        //     //save xml cache
        //     this.cache.save();
        //     // return newly created xml cache
        //     return domDocument;
        // }
    }

    private async getDomDocumentFromXmlCache(): Promise<XMLBuilder | null> {
        const dataset: Dataset = this.config.model;
        if (null == this.cache) {
            //$logger->debug(__METHOD__ . ' skipping cache for ' . get_class($model));
            // Log::debug(__METHOD__ . ' skipping cache for ' . get_class($dataset));
            Logger.warn(`__METHOD__ .  skipping cache for  ${dataset}`);
            return null;
        }
        // const dataset: Dataset = this.config.model;
        const actuallyCached: boolean = await DocumentXmlCache.hasValidEntry(dataset.id, dataset.server_date_modified);

        //no cache or no actual cache
        if (true != actuallyCached) {
            Logger.warn(" cache missing for " + "#" + dataset.id);
            return null;
        }

        //cache is actual return it for oai:
        //  Log::debug(__METHOD__ . ' cache hit for ' . get_class($dataset) . '#' . $dataset->id);
        try {
            // return $this->_cache->get($model->getId(), (int) $this->_strategy->getVersion());
            // const cache = await DocumentXmlCache.findOne({
            //     where: { document_id: dataset.id },
            // });
            if (this.cache) {
                return this.cache.getDomDocument();
            } else {
                Logger.warn(" Access to XML cache failed on " + dataset + "#" + dataset.id + ".  Trying to recover.");
                return null;
            }
            // return this.cache.getDomDocument();
        } catch (error) {
            Logger.warn(" Access to XML cache failed on " + dataset + "#" + dataset.id + ".  Trying to recover.");
            return null;
        }
    }
}

// export default XmlModel;

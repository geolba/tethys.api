export default class ResumptionToken {
    /**
     * Holds dcoument ids
     *
     * @var array
     */
    private _documentIds: number[] = [];

    /**
     * Holds metadata prefix information
     *
     * @var {string}
     */
    private _metadataPrefix = "";

    /**
     * Holds resumption id (only if token is stored)
     *
     * @var {string}
     */
    private _resumptionId = "";

    /**
     * Holds start postion
     *
     * @var {number}
     */
    private _startPosition = 0;

    /**
     * Holds total amount of document ids
     *
     * @var {number}
     */
    private _totalIds = 0;

    //#region properties

    get Key(): string {
        return this.MetadataPrefix + this.StartPosition + this.TotalIds;
    }

    /**
     *  Returns current holded document ids.
     *
     * @return array
     */
    public get DocumentIds(): number[] {
        return this._documentIds;
    }
    public set DocumentIds(idsToStore: number | number[]) {
        if (!Array.isArray(idsToStore)) {
            idsToStore = new Array(idsToStore);
        }
        this._documentIds = idsToStore;
    }

    /**
     * Returns metadata prefix information.
     *
     * @return string
     */
    public get MetadataPrefix(): string {
        return this._metadataPrefix;
    }
    public set MetadataPrefix(value) {
        this._metadataPrefix = value;
    }

    /**
     * Return setted resumption id after successful storing of resumption token.
     *
     * @return string
     */
    public get ResumptionId() {
        return this._resumptionId;
    }
    public set ResumptionId(resumptionId) {
        this._resumptionId = resumptionId;
    }

    /**
     * Returns start position.
     *
     * @return in
     */
    public get StartPosition() {
        return this._startPosition;
    }
    public set StartPosition(startPosition) {
        this._startPosition = startPosition;
    }

    /**
     * Returns total number of document ids for this request
     *
     * @return int
     */
    public get TotalIds() {
        return this._totalIds;
    }
    public set TotalIds(totalIds) {
        this._totalIds = totalIds;
    }

    //#endregion properties
}

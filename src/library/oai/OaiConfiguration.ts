import config from "../../config/oai.config";

export default class Configuration {
    /**
     * Hold path where to store temporary resumption token files.
     *
     * @var string
     */
    private _pathTokens = "";

    private _maxListIds = 15;

    /**
     * Return maximum number of listable identifiers per request.
     *
     * @return {number} Maximum number of listable identifiers per request.
     */
    public get maxListIds(): number {
        return this._maxListIds;
    }
    public set maxListIds(value: number) {
        this._maxListIds = value;
    }

    /**
     * Holds maximum number of records to list per request.
     *
     * @var number
     */
    private _maxListRecs = 15;

    /**
     * Return maximum number of listable records per request.
     *
     * @return {number} Maximum number of listable records per request.
     */
    public get maxListRecs() {
        return this._maxListRecs;
    }
    public set maxListRecs(value) {
        this._maxListRecs = value;
    }

    constructor() {
        this._maxListIds = config.max.listidentifiers as number;
        this._maxListRecs = config.max.listrecords as number;
        // $this->maxListIds = config('oai.max.listidentifiers');
        // $this->maxListRecs = config('oai.max.listrecords');
        // $this->pathTokens = config('app.workspacePath')
        // . DIRECTORY_SEPARATOR .'tmp'
        // . DIRECTORY_SEPARATOR . 'resumption';
    }

    /**
     * Return temporary path for resumption tokens.
     *
     * @returns {string} token path.
     */
    get getResumptionTokenPath(): string {
        return this._pathTokens;
    }

    /**
     * Return maximum number of listable records per request.
     *
     * @return {number} Maximum number of listable records per request.
     */
    // get getMaxListRecords(): number {
    //     return this._maxListRecs;
    // }
}

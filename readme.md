# TETHYS

TETHYS - Data Publisher for Geoscience Austria is a digital data library and a data publisher for earth system science. Data can be georeferenced in time (date/time) and space (latitude, longitude, depth/height).\
Please watch our website - [www.tethys.at](https://www.tethys.at)

----------

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.




## License

This project is licensed under the MIT License - see the [license](LICENSE) file for details
module.exports = {
    root: true,
    parser: "@typescript-eslint/parser",
    plugins: ["@typescript-eslint", "prettier"],
    extends: [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended",
        // // 'prettier/@typescript-eslint',
        // Then you need to add plugin:prettier/recommended as the last extension in your .eslintrc.json:
        // 'prettier/@typescript-eslint',
        "plugin:prettier/recommended",
    ],
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: "module",
    },
    env: {
        es6: true,
        node: true,
    },

    // "off" means 0 (turns the rule off completely)
    // "warn" means 1 (turns the rule on but won't make the linter fail)
    // "error" means 2 (turns the rule on and will make the linter fail)
    rules: {
        // "prettier/prettier": ["error", { "singleQuote": true }],
        "no-console": 0, // Remember, this means error!,
        "@typescript-eslint/no-unused-vars": "error",
        "@typescript-eslint/ban-ts-comment": "warn",
        "no-var": "error",
        semi: "error",
        //   indent: ['error', 4, { SwitchCase: 1 }],
        "no-multi-spaces": "error",
        "space-in-parens": "error",
        "no-multiple-empty-lines": "error",
        "prefer-const": "error",
        "@typescript-eslint/indent": ["error", 4],
        "prettier/prettier": ["error", { singleQuote: false }],

        // "arrow-body-style": "off",
        // "prefer-arrow-callback": "off"
    },
};

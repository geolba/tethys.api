<?xml version="1.0" encoding="utf-8"?>
<!--
     /**
     * This file is part of TETHYS. 
     *
     * LICENCE
     * TETHYS is free software; you can redistribute it and/or modify it under the
     * terms of the GNU General Public License as published by the Free Software
     * Foundation; either version 2 of the Licence, or any later version.
     * OPUS is distributed in the hope that it will be useful, but WITHOUT ANY
     * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
     * details. You should have received a copy of the GNU General Public License
     * along with OPUS; if not, write to the Free Software Foundation, Inc., 51
     * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
     *
     * @category    Application
     * @package     Module Oai
     * @author      Arno Kaimbacher <arno.kaimbacher@geologie.ac.at>
     * @copyright   Copyright (c) 2022, GBA TETHYS development team
     * @license     http://www.gnu.org/licenses/gpl.html General Public License
     */
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:fn="http://example.com/functions"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:gmd="http://www.isotc211.org/2005/gmd"
                xmlns:gco="http://www.isotc211.org/2005/gco"                
                expand-text="yes"
                version="3.0">
    
    <xsl:output method="xml" encoding="utf-8" indent="yes"/>
    <xsl:mode on-no-match="shallow-copy"/> 
    
    <xsl:import href="functions.xslt"/>
    
    <!-- variables  -->
    <!-- <xsl:variable name="datacite-identifier_org" select="//*[name() = 'Identifier']"/> -->
    <!-- <xsl:variable name="datacite-identifier" select="//*[name() = 'Identifier'][@Type = 'Doi']"/>    -->
    <xsl:variable name="fileIdentifierPrefix" select="string('at.tethys.dataset')"/>    
    <xsl:variable name="datacentre">
        <xsl:choose>           
            <xsl:when test="string-length(normalize-space(@CreatingCorporation)) > 0">
                <xsl:value-of select="normalize-space(@CreatingCorporation)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="string('Tethys RDR')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <!-- main template for is 19139 -->    
    <xsl:template match="Rdr_Dataset" mode="iso19139">
        <!-- <gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gml="http://www.opgeris.net/gml/3.2" xmlns:gmx="http://www.isotc211.org/2005/gmx"
                         xsi:schemaLocation="http://www.isotc211.org/2005/gmd https://www.isotc211.org/2005/gmd/metadataApplication.xsd http://www.isotc211.org/2005/gmd  http://schemas.opengis.net/iso/19139/20060504/gmd/gmd.xsd http://www.isotc211.org/2005/gmx http://schemas.opengis.net/iso/19139/20060504/gmx/gmx.xsd"> -->
        <gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gmx="http://www.isotc211.org/2005/gmx" 
        xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gsr="http://www.isotc211.org/2005/gsr" xmlns:gmi="http://www.isotc211.org/2005/gmi" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink" 
        xsi:schemaLocation="http://www.isotc211.org/2005/gmd http://schemas.opengis.net/iso/19139/20060504/gmd/gmd.xsd http://www.isotc211.org/2005/gmx http://schemas.opengis.net/iso/19139/20060504/gmx/gmx.xsd http://www.isotc211.org/2005/srv http://schemas.opengis.net/iso/19139/20060504/srv/srv.xsd">
            
            <!-- gmd:fileIdentifier -->
            <xsl:if test="Identifier">
                <xsl:apply-templates select="Identifier" mode="iso19139" />
            </xsl:if>
            
            <!-- Ressourensprache in der Eingabemaske von Tethys nicht erfasst-->
            <gmd:language>
                <gmd:LanguageCode codeList="http://www.loc.gov/standards/iso639-2/" codeListValue="ger"/>                
            </gmd:language>
            <gmd:characterSet>
                <gmd:MD_CharacterSetCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_CharacterSetCode" codeListValue="utf8"/>
            </gmd:characterSet> 
            
            <!--gmd:hierarchyLevel template-->
            <xsl:call-template name="hierarchylevel"/>
            
            <gmd:hierarchyLevelName>
                <gco:CharacterString>
                    <xsl:text>Dataset</xsl:text>
                </gco:CharacterString>
            </gmd:hierarchyLevelName>
            
            <!-- gmd:contact - The contact for the metadata - static value assigned in template at the top of this file; -->
            <xsl:call-template name="metadatacontact"/>
            
            <!-- gmd:dateStamp -->
            <xsl:call-template name="datestamp"/>
            
            <gmd:metadataStandardName>
                <gco:CharacterString>ISO 19139 Geographic Information - Metadata - Implementation Specification</gco:CharacterString>
            </gmd:metadataStandardName>
            <gmd:metadataStandardVersion>
                <gco:CharacterString>2007</gco:CharacterString>
            </gmd:metadataStandardVersion>
            
            <gmd:referenceSystemInfo>
                <gmd:MD_ReferenceSystem>
                    <gmd:referenceSystemIdentifier>
                        <gmd:RS_Identifier>
                            <gmd:code>
                                <!-- <gmx:Anchor xlink:href="https://www.opengis.net/def/crs/EPSG/0/25832">EPSG:25832</gmx:Anchor> -->                              
                                <gco:CharacterString>https://www.opengis.net/def/crs/EPSG/0/31287</gco:CharacterString>
                            </gmd:code>
                            <gmd:version>
                                <gco:CharacterString>6.11.2</gco:CharacterString>
                            </gmd:version>
                        </gmd:RS_Identifier>
                    </gmd:referenceSystemIdentifier>
                </gmd:MD_ReferenceSystem>
            </gmd:referenceSystemInfo>
            
            <gmd:identificationInfo>
                <gmd:MD_DataIdentification>                    
                    
                    <!-- gmd:citation> -->
                    <gmd:citation>
                        <gmd:CI_Citation>
                            <!-- gmd:title -->
                            <xsl:call-template name="title"/>
                            
                            <!-- <gmd:alternateTitle -->
                            <xsl:apply-templates select="TitleAdditional" mode="iso19139" />
                            
                            <!-- gmd:date -->
                            <xsl:call-template name="resourcedates"/>
                            
                            <!-- gmd:identifier -->
                            <!-- stower 2022, identifer gba https://doi.org/10.24341/tethys.53 -->
                            <gmd:identifier>
                                <xsl:for-each select="*[name() = 'Identifier']"> 
                                    <!-- <xsl:variable name="datacite-identifier" select="Identifier[@Type = 'Doi']"/>  -->                                              
                                    <xsl:variable name="datacite-identifier" select=".[@Type = 'Doi']"/>                               
                                    <xsl:if test="string-length(.[@Type = 'Doi']/@Value) &gt; 0 and count([.[@Type = 'Doi']/@Type = 'Doi']) &gt; 0">
                                        <gmd:MD_Identifier>
                                            <gmd:code>
                                                <gco:CharacterString>   
                                                    <xsl:choose>
                                                        <xsl:when test="starts-with(.[@Type = 'Doi']/@Value, 'doi:') or contains(.[@Type = 'Doi']/@Value, 'doi.org')">
                                                            <xsl:value-of select=".[@Type = 'Doi']/@Value"/>
                                                        </xsl:when>
                                                        <xsl:when test="count(.[@Type = 'Doi']) &gt; 0 and $datacentre='Tethys RDR'">
                                                            <xsl:value-of select="concat(
                                                                    'https://doi.org/',
                                                                    string(.[@Type = 'Doi']/@Value)
                                                                )"/>                                           
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:value-of select=".[@Type = 'Doi']/@Value"/>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </gco:CharacterString>
                                            </gmd:code>
                                        </gmd:MD_Identifier>
                                    </xsl:if>      
                                </xsl:for-each>  
                            </gmd:identifier>
                            
                            <!-- gmd:citedResponsibleParty - creator -->
                            <xsl:apply-templates select="PersonAuthor" mode="iso19139">
                                <xsl:sort select="@SortOrder"/>               
                            </xsl:apply-templates>
                            
                            <!-- gmd:citedResponsibleParty - contributor -->
                            <xsl:apply-templates select="PersonContributor" mode="iso19139">
                                <xsl:sort select="@SortOrder"/>               
                            </xsl:apply-templates>
                            
                            <!-- publisher -->
                            <gmd:citedResponsibleParty>
                                <xsl:call-template name="ci_responsibleparty">
                                    <xsl:with-param name="organisation">
                                        <xsl:value-of select="@CreatingCorporation"/>
                                    </xsl:with-param>
                                    <xsl:with-param name="role">publisher</xsl:with-param>
                                </xsl:call-template>
                            </gmd:citedResponsibleParty>     
                        </gmd:CI_Citation>
                    </gmd:citation>
                    
                    <!-- gmd:abstract -->
                    <xsl:call-template name="abstract"/>
                    
                    <gmd:status>
                        <gmd:MD_ProgressCode codeList="http://www.isotc211.org/2005/resources/Codelist/gmxCodelists.xml#MD_ProgressCode" codeListValue="Complete" codeSpace="ISOTC211/19115"/>
                    </gmd:status>
                    
                    <!-- gmd:pointOfContact custodian -->
                    <xsl:call-template name="pointofcontact_custodian"/>
                    
                    <!-- gmd:pointOfContact originator -->
                    <xsl:call-template name="pointofcontact_originator"/>
                    
                    <!-- gmd:resourceMaintenance -->
                    <gmd:resourceMaintenance>
                        <gmd:MD_MaintenanceInformation>
                            <gmd:maintenanceAndUpdateFrequency>
                                <gmd:MD_MaintenanceFrequencyCode codeList="http://schemas.opengis.net/iso/19139/20070417/resources/codelist/gmxCodelists.xml#MD_MaintenanceFrequencyCode" codeListValue="asNeeded">asNeeded</gmd:MD_MaintenanceFrequencyCode>
                            </gmd:maintenanceAndUpdateFrequency>
                        </gmd:MD_MaintenanceInformation>
                    </gmd:resourceMaintenance>
                    
                    <!-- gmd:credit -->
                    <!-- <xsl:if test="count(//*[name() = 'fundingReferences']) &gt; 0">
                         <xsl:for-each select="//*[local-name() = 'fundingReferences']/*[name() = 'fundingReference']">
                         <gmd:credit>
                         <gco:CharacterString>
                         <xsl:value-of select="normalize-space(concat(string('funderName:'), string(*[name() = 'funderName'])))"/>
                         <xsl:if test="count(*[local-name() = 'funderIdentifier']) &gt; 0">
                         <xsl:text>
                         </xsl:text>
                         <xsl:value-of select="normalize-space(concat(string('funderIdentifier:'), string(*[name() = 'funderIdentifier'])))"/>
                         <xsl:value-of select="normalize-space(concat(string('; IDType:'), string(*[name() = 'funderIdentifier']/@funderIdentifierType)))"/>
                         </xsl:if>
                         <xsl:if test="count(*[name() = 'awardNumber']) &gt; 0">
                         <xsl:text>
                         </xsl:text>
                         <xsl:value-of select="normalize-space(concat(string('awardNumber:'), string(*[name() = 'awardNumber'])))"/>
                         </xsl:if>
                         <xsl:if test="count(*[name() = 'awardTitle']) &gt; 0">
                         <xsl:text>
                         </xsl:text>
                         <xsl:value-of select="normalize-space(concat(string('awardTitle:'), string(*[name() = 'awardTitle'])))"/>
                         </xsl:if>
                         </gco:CharacterString>
                         </gmd:credit>
                         </xsl:for-each>
                         </xsl:if> -->
                    
                    <!-- <gmd:graphicOverview> -->
                    <xsl:call-template name="browseGraphictethys"/>
                    
                    <!-- gmd:descriptiveKeywords without thesaurus-->
                    <xsl:call-template name="freekeywords"/>
                    
                    <xsl:call-template name="datacenter_keyword"/>
                    
                    <!-- gmd:resourceConstraints -->
                    <xsl:apply-templates select="Licence" mode="iso19139" />
                    <gmd:resourceConstraints>
                        <gmd:MD_SecurityConstraints>
                            <gmd:classification>
                                <gmd:MD_ClassificationCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_ClassificationCode" codeListValue="unclassified"/>
                            </gmd:classification>
                        </gmd:MD_SecurityConstraints>
                    </gmd:resourceConstraints>
                    
                    <!-- relatedresources mapped to gmd:aggregationInfo, -->
                    <!-- <xsl:call-template name="relatedResources">
                         <xsl:with-param name="relres" select="//*[local-name() = 'relatedIdentifiers']"/>
                         </xsl:call-template> -->
                    <!-- <gmd:aggregationInfo> -->
                    <xsl:apply-templates select="Reference" mode="iso19139" />
                    
                    <gmd:spatialResolution>
                        <xsl:variable name="mainTitle">
                            <!-- <xsl:for-each select="$datacite-titles/*[name() = 'title']"> -->
                            <xsl:for-each select="*[name() = 'TitleMain']">
                                <xsl:if test="normalize-space(@Type) = 'Main'">
                                    <xsl:value-of select="@Value"/>                                    
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:variable>
                        <gmd:MD_Resolution>
                            <xsl:choose>
                                <xsl:when test="contains($mainTitle, '50.000')">                                
                                    <gmd:equivalentScale>
                                        <gmd:MD_RepresentativeFraction>
                                            <gmd:denominator>
                                                <!-- <gco:Integer>{$mainTitle}</gco:Integer> -->
                                                <gco:Integer>50000</gco:Integer>
                                            </gmd:denominator>
                                        </gmd:MD_RepresentativeFraction>
                                    </gmd:equivalentScale>
                                </xsl:when>
                                <xsl:otherwise>                                    
                                    <gmd:distance>
                                        <gco:Distance uom="m">-1</gco:Distance>
                                    </gmd:distance>
                                </xsl:otherwise>
                            </xsl:choose>                            
                        </gmd:MD_Resolution>
                    </gmd:spatialResolution>
                    
                    <!-- assume ger for now, stower 2022 -->
                    <gmd:language>
                        <gmd:LanguageCode codeList="http://www.loc.gov/standards/iso639-2/" codeListValue="ger"/>
                    </gmd:language>
                    
                    <!-- ISOTopic19115, stower 2022 -->
                    <gmd:topicCategory>
                        <gmd:MD_TopicCategoryCode>geoscientificInformation</gmd:MD_TopicCategoryCode>
                    </gmd:topicCategory>
                    
                    <!-- gmd:geographicElement> -->
                    <xsl:call-template name="spatialcoverage"/>
                    
                    <!-- <xsl:call-template name="temporalcoverage"/> -->                 
                    
                    
                </gmd:MD_DataIdentification>
            </gmd:identificationInfo>
            
            <!-- gmd:distributionInfo -->
            <gmd:distributionInfo>
                <gmd:MD_Distribution>
                    
                    <!-- kaiarn 2022  gmd:distributionFormat -->
                    <xsl:apply-templates select="File" mode="iso19139" />
                    
                    <!-- gmd:distributor -->
                    <xsl:call-template name="distributor" /> 
                    
                    <!-- gmd:transferOptions -->
                    <gmd:transferOptions>
                        <gmd:MD_DigitalTransferOptions>
                            <!-- <xsl:call-template name="size"/> -->
                            
                            <!-- <gmd:onLine> -->
                            <xsl:call-template name="datacite_identifier" />  
                            
                            <!-- <gmd:onLine> -->
                            <xsl:call-template name="alternate_identifier" />                            
                        </gmd:MD_DigitalTransferOptions>
                    </gmd:transferOptions>
                    
                </gmd:MD_Distribution>
            </gmd:distributionInfo>
            
            <!-- gmd:dataQualityInfo -->
            <xsl:call-template name="data_quality"/>
            
            <!-- <gmd:metadataMaintenance> -->
            <xsl:call-template name="metadata_maintenance" />
            
        </gmd:MD_Metadata> 
    </xsl:template>
    
    <!-- further helper templates -->
    
    <!-- gmd:resourceConstraints -->
    <xsl:template match="Licence" mode="iso19139">
        <gmd:resourceConstraints xlink:href="https://creativecommons.org/licenses/by/4.0/deed.en">
            <gmd:MD_Constraints>
                <gmd:useLimitation>
                    <gco:CharacterString>
                        <xsl:value-of select="string(@NameLong)"/>
                    </gco:CharacterString>
                </gmd:useLimitation>
            </gmd:MD_Constraints>
        </gmd:resourceConstraints>
        <gmd:resourceConstraints>
            <gmd:MD_LegalConstraints>
                <gmd:accessConstraints>
                    <gmd:MD_RestrictionCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_RestrictionCode" codeListValue="otherRestrictions"/>
                </gmd:accessConstraints>
                <gmd:otherConstraints>
                    <gco:CharacterString>
                        <xsl:value-of select="string(@NameLong)"/>
                    </gco:CharacterString>
                </gmd:otherConstraints>
            </gmd:MD_LegalConstraints>
        </gmd:resourceConstraints>
    </xsl:template>
    
    
    
    <!-- gmd:fileIdentifier -->
    <xsl:template match="Identifier" mode="iso19139">
        <gmd:fileIdentifier>
            <gco:CharacterString>
                <!-- <xsl:value-of select="@Value" />         -->
                <xsl:choose>
                    <xsl:when test="string-length(@Value) &gt; 0 and count([@Type = 'Doi']) &gt; 0">
                        <xsl:value-of select="concat($fileIdentifierPrefix, normalize-space(substring-after(@Value, '/tethys.')))"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="string(@Value)"/>
                    </xsl:otherwise>
                </xsl:choose>
            </gco:CharacterString>
        </gmd:fileIdentifier>  
    </xsl:template>
    
    <!-- gmd:dateStamp template-->
    <xsl:template name="datestamp">
        <gmd:dateStamp>    
            <!-- define vriable theDate -->
            <xsl:variable name="theDate">
                <xsl:choose>
                    <xsl:when test="ServerDateModified">
                        <xsl:value-of select="concat(
                                ServerDateModified/@Year, '-',
                                format-number(number(ServerDateModified/@Month),'00'), '-',
                                format-number(number(ServerDateModified/@Day),'00')                            
                            )"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat(
                                ServerDatePublished/@Year, '-',
                                format-number(number(ServerDatePublished/@Month),'00'), '-',
                                format-number(number(ServerDatePublished/@Day),'00')                              
                            )" />                       
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>            
            <gco:Date>                                      
                <xsl:value-of select="string($theDate)"/>
            </gco:Date>            
        </gmd:dateStamp>
    </xsl:template>
    
    <!-- gmd:title -->
    <xsl:template name="title">
        <gmd:title>
            <gco:CharacterString>
                <xsl:for-each select="*[name() = 'TitleMain' or name() = 'TitleAdditional']">
                    <xsl:choose>
                        <xsl:when test="string(@Type) = 'Main'">
                            <xsl:value-of select="@Value"/>
                        </xsl:when>
                        <xsl:when test="string(@Type) = 'Sub' and @Value != ''">
                            <xsl:value-of select="concat('; Subtitle: ', @Value)"/>
                        </xsl:when>
                        <xsl:when test="string(@Type) = 'Translated' and @Value != ''">
                            <xsl:value-of select="concat('; Translated title: ', @Value)"/>
                        </xsl:when>
                        <xsl:when test="string(@Type) = 'Other' and @Value != ''">
                            <xsl:value-of select="concat('; Other title: ', @Value)"/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </gco:CharacterString>
        </gmd:title>
    </xsl:template>
    
    <!-- <gmd:alternateTitle> -->
    <xsl:template match="TitleAdditional" mode="iso19139">
        <xsl:if test="string(@Type) = 'Alternative' and @Value != ''">
            <gmd:alternateTitle>
                <gco:CharacterString>
                    <xsl:value-of select="string(@Value)"/>
                </gco:CharacterString>
            </gmd:alternateTitle>
        </xsl:if>
    </xsl:template>
    
    <!-- <gmd:citedResponsibleParty> -->
    <xsl:template match="PersonAuthor" mode="iso19139">
        <gmd:citedResponsibleParty>
            <xsl:variable name="http-uri">
                <xsl:choose>           
                    <xsl:when test="string-length(@IdentifierOrcid) > 0">
                        <xsl:value-of select="concat(
                                'http://orcid.org/',
                                string(@IdentifierOrcid)
                            )"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="string('')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <!-- set attribute xlink:href -->
            <xsl:if test="$http-uri != ''">
                <xsl:attribute name="xlink:href">
                    <xsl:value-of select="$http-uri"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:variable name="nameidscheme">
                <xsl:choose>
                    <xsl:when test="starts-with($http-uri, 'http://orcid.org/')">
                        <xsl:value-of select="string('ORCID')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="string('')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="email">
                <xsl:choose>
                    <xsl:when test="@Email != ''">                           
                        <xsl:value-of select="@Email"/>                                
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="string('')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="affiliation">
                <xsl:choose>
                    <xsl:when test="@NameType = 'Personal'">                           
                        <xsl:value-of select="string('GBA')"/>                                
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="string('')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="creatorname">                
                <xsl:value-of select="@LastName" />
                <xsl:if test="@FirstName != ''">
                    <xsl:text>, </xsl:text>
                </xsl:if>
                <xsl:value-of select="@FirstName" />
                <xsl:if test="@AcademicTitle != ''">
                    <xsl:text> (</xsl:text>
                    <xsl:value-of select="@AcademicTitle" />
                    <xsl:text>)</xsl:text>
                </xsl:if>
            </xsl:variable>
            <xsl:variable name="namestring">
                <xsl:choose>
                    <xsl:when test="normalize-space(@LastName) != ''">
                        <xsl:value-of select="
                            concat(
                                normalize-space(@LastName),
                                ', ',
                                normalize-space(@FirstName))"/>
                    </xsl:when>
                    <!-- <xsl:when test="normalize-space(*[local-name() = 'creatorName']) != ''">
                         <xsl:value-of select="normalize-space(*[local-name() = 'creatorName'])"/>
                         </xsl:when> -->
                    <xsl:otherwise>
                        <xsl:value-of select="string('missing')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:call-template name="ci_responsibleparty">
                <xsl:with-param name="individual">
                    <xsl:value-of select="$namestring"/>
                </xsl:with-param>                   
                <xsl:with-param name="personidtype">
                    <xsl:value-of select="$nameidscheme"/>
                </xsl:with-param>
                <xsl:with-param name="organisation">
                    <xsl:value-of select="$affiliation"/>
                </xsl:with-param>
                <xsl:with-param name="role">author</xsl:with-param>
                <xsl:with-param name="email">
                    <xsl:value-of select="$email"/>
                </xsl:with-param>
                <xsl:with-param name="datacite-creatorname">
                    <xsl:value-of select="$creatorname"/>
                </xsl:with-param>
            </xsl:call-template>
            <!-- <xsl:value-of select="$namestring"/> -->
        </gmd:citedResponsibleParty>
    </xsl:template>
    
    <xsl:template match="PersonContributor" mode="iso19139">
        <xsl:variable name="dcrole" select="normalize-space(./@ContributorType)"/>
        <xsl:variable name="role">
            <xsl:choose>
                <xsl:when test="$dcrole = 'ContactPerson'">pointOfContact</xsl:when>
                <xsl:when test="$dcrole = 'DataCollector'">collaborator</xsl:when>
                <xsl:when test="$dcrole = 'DataCurator'">custodian</xsl:when>
                <xsl:when test="$dcrole = 'DataManager'">custodian</xsl:when>
                <xsl:when test="$dcrole = 'Distributor'">originator</xsl:when>
                <xsl:when test="$dcrole = 'Editor'">editor</xsl:when>
                <xsl:when test="$dcrole = 'Funder'">funder</xsl:when>
                <xsl:when test="$dcrole = 'HostingInstitution'">distributor</xsl:when>
                <xsl:when test="$dcrole = 'ProjectLeader'">collaborator</xsl:when>
                <xsl:when test="$dcrole = 'ProjectManager'">collaborator</xsl:when>
                <xsl:when test="$dcrole = 'ProjectMember'">collaborator</xsl:when>
                <xsl:when test="$dcrole = 'ResearchGroup'">collaborator</xsl:when>
                <xsl:when test="$dcrole = 'Researcher'">collaborator</xsl:when>
                <xsl:when test="$dcrole = 'RightsHolder'">rightsHolder</xsl:when>
                <xsl:when test="$dcrole = 'Sponsor'">funder</xsl:when>
                <xsl:when test="$dcrole = 'WorkPackageLeader'">collaborator</xsl:when>
                <xsl:otherwise>contributor</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <gmd:citedResponsibleParty>
            <xsl:variable name="http-uri">
                <xsl:choose>           
                    <xsl:when test="string-length(@IdentifierOrcid) > 0">
                        <xsl:value-of select="concat(
                                'http://orcid.org/',
                                string(@IdentifierOrcid)
                            )"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="string('')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <!-- set attribute xlink:href -->
            <xsl:if test="$http-uri != ''">
                <xsl:attribute name="xlink:href">
                    <xsl:value-of select="$http-uri"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:variable name="nameidscheme">
                <xsl:choose>
                    <xsl:when test="starts-with($http-uri, 'http://orcid.org/')">
                        <xsl:value-of select="string('ORCID')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="string('')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="email">
                <xsl:choose>
                    <xsl:when test="@Email != ''">                           
                        <xsl:value-of select="@Email"/>                                
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="string('')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="affiliation">
                <xsl:choose>
                    <xsl:when test="@NameType = 'Personal'">                           
                        <xsl:value-of select="string('GBA')"/>                                
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="string('')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="contrnamestring">
                <xsl:choose>
                    <xsl:when test="normalize-space(@LastName) != ''">
                        <xsl:value-of select="
                            concat(
                                normalize-space(@LastName),
                                ', ',
                                normalize-space(@FirstName))"/>
                    </xsl:when>                       
                    <xsl:otherwise>
                        <xsl:value-of select="string('missing')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:call-template name="ci_responsibleparty">
                <xsl:with-param name="individual">
                    <xsl:value-of select="$contrnamestring"/>
                </xsl:with-param>
                <xsl:with-param name="httpuri">
                    <xsl:value-of select="$http-uri"/>
                </xsl:with-param>
                <xsl:with-param name="personidtype">
                    <xsl:value-of select="$nameidscheme"/>
                </xsl:with-param>
                <xsl:with-param name="organisation">
                    <xsl:value-of select="$affiliation"/>
                </xsl:with-param>
                <xsl:with-param name="position">
                    <xsl:value-of select="$dcrole"/>
                </xsl:with-param>
                <xsl:with-param name="email">
                    <xsl:value-of select="$email"/>
                </xsl:with-param>
                <xsl:with-param name="role" select="$role"/>
            </xsl:call-template>
        </gmd:citedResponsibleParty>
    </xsl:template>
    
    <xsl:variable name="nl" select="string('&#10;')"/>
    <!-- gmd:abstract -->
    <xsl:template name="abstract">
        <gmd:abstract>
            <gco:CharacterString>
                <xsl:for-each select="*[name() = 'TitleAbstract' or name() = 'TitleAbstractAdditional']">
                    <!-- <xsl:text>&#10;</xsl:text> -->
                    <xsl:choose>
                        <xsl:when test="string(@Type) = 'Abstract'">
                            <!-- <xsl:value-of select="@Value"/> -->
                            <xsl:value-of select="concat('&#10;Abstract: ', string(@Value))"/>
                        </xsl:when>
                        <xsl:when test="string(@Type) = 'Translated'">                           
                            <xsl:value-of select="concat('&#10;Translated Description: ', string(@Value))"/>
                        </xsl:when>
                        <xsl:when test="string(@Type) = 'SeriesInformation' and @Value != ''">                           
                            <xsl:value-of select="concat('&#10;Series Information: ', string(@Value))"/>
                        </xsl:when>
                        <xsl:when test="string(@Type) = 'TableOfContents' and @Value != ''">                          
                            <xsl:value-of select="concat('&#10;Table of Contents: ', string(@Value))"/>
                        </xsl:when>
                        <xsl:when test="string(@Type) = 'TechnicalInfo' and @Value != ''">                          
                            <xsl:value-of select="concat('&#10;Technical Info: ', string(@Value))"/>
                        </xsl:when>
                        <xsl:when test="string(@Type) = 'Methods' and @Value != ''">                          
                            <xsl:value-of select="concat('&#10;Methods: ', string(@Value))"/>
                        </xsl:when>
                        <xsl:when test="string(@Type) = 'Other' and @Value != ''">                           
                            <xsl:if test="not(contains(string(@Value), 'Related publications:'))">                              
                                <xsl:value-of select="concat('&#10;Other Description: ', string(@Value))"/>
                            </xsl:if>
                        </xsl:when>
                    </xsl:choose>                   
                </xsl:for-each>   
                <!-- <xsl:value-of select="$nl" disable-output-escaping="no"/> -->
            </gco:CharacterString>
        </gmd:abstract>
    </xsl:template>
    
    <!-- <gmd:aggregationInfo> -->
    <xsl:template match="Reference" mode="iso19139">        
        <xsl:if test="
            not(contains(normalize-space(string(@Value)), 'missing')) and
            not(contains(normalize-space(string(@Value)), 'unknown'))">
            <gmd:aggregationInfo>
                <gmd:MD_AggregateInformation>
                    <gmd:aggregateDataSetIdentifier>
                        <gmd:RS_Identifier>
                            <gmd:code>
                                <gco:CharacterString>
                                    <xsl:value-of select="normalize-space(string(@Value))"/>
                                </gco:CharacterString>
                            </gmd:code>
                            <gmd:codeSpace>
                                <gco:CharacterString>
                                    <xsl:value-of select="normalize-space(string(@Type))"/>
                                </gco:CharacterString>
                            </gmd:codeSpace>
                        </gmd:RS_Identifier>
                    </gmd:aggregateDataSetIdentifier>
                    <gmd:associationType>
                        <xsl:element name="gmd:DS_AssociationTypeCode">
                            <xsl:attribute name="codeList">
                                <xsl:value-of select="string('http://datacite.org/schema/kernel-4')"/>
                            </xsl:attribute>
                            <xsl:attribute name="codeListValue">
                                <xsl:value-of select="normalize-space(string(@Type))"/>
                            </xsl:attribute>
                            <xsl:value-of select="normalize-space(string(@Type))"/>
                        </xsl:element>
                    </gmd:associationType>
                </gmd:MD_AggregateInformation>
            </gmd:aggregationInfo>
        </xsl:if>        
    </xsl:template>
    
    <xsl:template match="File" mode="iso19139">
        <xsl:if test="string-length(normalize-space(string(@MimeType))) > 0">
            <gmd:distributionFormat>
                <gmd:MD_Format>
                    <gmd:name>
                        <gco:CharacterString>
                            <xsl:value-of select="normalize-space(string(@MimeType))"/>
                        </gco:CharacterString>
                    </gmd:name>
                    <gmd:version gco:nilReason="missing"/>
                </gmd:MD_Format>
            </gmd:distributionFormat>
        </xsl:if>
    </xsl:template>
    
    
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:fn="http://example.com/functions"
                xmlns:gml="http://www.opgeris.net/gml/3.2"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:gmx="http://www.isotc211.org/2005/gmx"
                xmlns:gmd="http://www.isotc211.org/2005/gmd"
                xmlns:gco="http://www.isotc211.org/2005/gco"
                exclude-result-prefixes="#all"
                expand-text="yes"
                version="3.0">
    
    <xsl:variable name="resourcetype">
        <!-- <xsl:value-of select="//*[local-name() = 'resourceType']/@resourceTypeGeneral"/> -->
        <xsl:text>Dataset</xsl:text>
    </xsl:variable>
    <xsl:variable name="MDScopecode">
        <xsl:choose>
            <xsl:when test="$resourcetype = 'Dataset'">dataset</xsl:when>
            <xsl:when test="$resourcetype = 'Software'">software</xsl:when>
            <xsl:when test="$resourcetype = 'Service'">service</xsl:when>
            <xsl:when test="$resourcetype = 'Model'">model</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$resourcetype"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="MDScopelist">
        <xsl:choose>
            <xsl:when test="$resourcetype = 'Dataset' or $resourcetype = 'Software' or $resourcetype = 'Service' or $resourcetype = 'Model'">http://www.isotc211.org/2005/resources/Codelist/gmxCodelists.xml#MD_ScopeCode</xsl:when>
            <xsl:otherwise>http://datacite.org/schema/kernel-4</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>  
    <xsl:variable name="datacentre">
        <xsl:choose>           
            <xsl:when test="string-length(normalize-space(@CreatingCorporation)) > 0">
                <xsl:value-of select="normalize-space(@CreatingCorporation)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="string('Tethys RDR')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable> 
    
    
    
    <!-- <xsl:function name="fn:FormatDate" as="xs:string">        
         <xsl:param name="DateTime" as="xs:string" />
         <xsl:variable name="date">
         <xsl:value-of select="substring-before($DateTime,'T')" />
         </xsl:variable>
         <xsl:if test="string-length($date) = 10">
         <xsl:value-of select="$date"/>
         </xsl:if>
         </xsl:function> -->
    
    
    <!--gmd:hierarchyLevel template-->
    <xsl:template name="hierarchylevel">        
        <gmd:hierarchyLevel>
            <gmd:MD_ScopeCode>
                <xsl:attribute name="codeList">
                    <xsl:value-of select="$MDScopelist"/>
                </xsl:attribute>
                <xsl:attribute name="codeListValue">
                    <xsl:value-of select="$MDScopecode"/>
                </xsl:attribute>
                <xsl:value-of select="$MDScopecode"/>
            </gmd:MD_ScopeCode>
        </gmd:hierarchyLevel>
    </xsl:template>
    
    <!-- gmd:contact template-->
    <xsl:template name="metadatacontact">
        <gmd:contact>
            <gmd:CI_ResponsibleParty>
                <gmd:individualName>
                    <gco:CharacterString>Tethys RDR</gco:CharacterString>
                </gmd:individualName>
                <gmd:organisationName>
                    <gco:CharacterString>Tethys RDR</gco:CharacterString>
                </gmd:organisationName>
                <gmd:positionName>
                    <gco:CharacterString>Research Data Repository</gco:CharacterString>
                </gmd:positionName>
                <gmd:contactInfo>
                    <gmd:CI_Contact>
                        <gmd:address>
                            <gmd:CI_Address>
                                <gmd:deliveryPoint>
                                    <gco:CharacterString>Neulinggasse 38</gco:CharacterString>
                                </gmd:deliveryPoint>
                                <gmd:city>
                                    <gco:CharacterString>Vienna</gco:CharacterString>
                                </gmd:city>
                                <gmd:administrativeArea>
                                    <gco:CharacterString>Vienna</gco:CharacterString>
                                </gmd:administrativeArea>
                                <gmd:postalCode>
                                    <gco:CharacterString>1030</gco:CharacterString>
                                </gmd:postalCode>
                                <gmd:country>
                                    <gco:CharacterString>AT</gco:CharacterString>
                                </gmd:country>
                                <gmd:electronicMailAddress>
                                    <gco:CharacterString>repository@geologie.ac.at</gco:CharacterString>
                                </gmd:electronicMailAddress>
                            </gmd:CI_Address>
                        </gmd:address>
                        <!--  only allow one online resource; choose the logo...-->
                        <gmd:onlineResource>
                            <gmd:CI_OnlineResource>
                                <gmd:linkage>
                                    <gmd:URL>https://tethys.at</gmd:URL>
                                </gmd:linkage>
                                <gmd:function>
                                    <gmd:CI_OnLineFunctionCode codeList="http://www.isotc211.org/2005/resources/Codelist/gmxCodelists.xml#CI_OnLineFunctionCode" codeListValue="information">information</gmd:CI_OnLineFunctionCode>
                                </gmd:function>
                            </gmd:CI_OnlineResource>
                        </gmd:onlineResource>
                    </gmd:CI_Contact>
                </gmd:contactInfo>
                <gmd:role>
                    <gmd:CI_RoleCode codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_RoleCode" codeListValue="pointOfContact">pointOfContact</gmd:CI_RoleCode>
                </gmd:role>
            </gmd:CI_ResponsibleParty>
        </gmd:contact>
    </xsl:template>
    
    <!-- retrieves basic reference dates of the resource
         Handles created date, publication (Available) date, and revision date-->
    <!-- stower 2022, Created auf created geändert -->
    <xsl:template name="resourcedates">
        <xsl:if test="*[name() = 'CreatedAt']">
            <gmd:date>
                <gmd:CI_Date>
                    <gmd:date>
                        <!-- <xsl:variable name="theDate" select="normalize-space(*[name() = 'CreatedAt'])"/> -->
                        <xsl:variable name="theDate">               
                            <xsl:value-of select="normalize-space(concat(
                                        CreatedAt/@Year, '-',
                                        format-number(number(CreatedAt/@Month),'00'), '-',
                                        format-number(number(CreatedAt/@Day),'00'), 'T',
                                        format-number(number(CreatedAt/@Hour),'00'), ':',
                                        format-number(number(CreatedAt/@Minute),'00'), ':',
                                        format-number(number(CreatedAt/@Second),'00'), 'Z'                          
                                    ))"/>   
                        </xsl:variable>   
                        <!-- ieda dataCite labels metadata update date in the date string to avoid ambiguity -->
                        <xsl:choose>
                            <xsl:when test="$theDate != ''">
                                <xsl:choose>
                                    <xsl:when test="contains($theDate, 'T')">
                                        <gco:DateTime>
                                            <xsl:value-of select="$theDate"/>
                                        </gco:DateTime>
                                    </xsl:when>
                                    <xsl:when test="contains($theDate, ' ')">
                                        <gco:DateTime>
                                            <xsl:value-of select="translate($theDate, ' ', 'T')"/>
                                        </gco:DateTime>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <gco:Date>
                                            <xsl:value-of select="$theDate"/>
                                        </gco:Date>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <!-- other write the output of missing -->
                            <xsl:otherwise>
                                <xsl:attribute name="gco:nilReason">
                                    <xsl:value-of select="string('created date missing')"/>
                                </xsl:attribute>
                            </xsl:otherwise>
                        </xsl:choose>
                    </gmd:date>
                    <gmd:dateType>
                        <gmd:CI_DateTypeCode codeList="http://www.isotc211.org/2005/resources/Codelist/gmxCodelists.xml#CI_DateTypeCode" codeListValue="creation">creation</gmd:CI_DateTypeCode>
                    </gmd:dateType>
                </gmd:CI_Date>
            </gmd:date>
        </xsl:if>
        <xsl:choose>
            <!-- Take 'Available' (embargo) date first because that is YYYY-MM-DD -->
            <xsl:when test="*[local-name() = 'EmbargoDate']">
                <gmd:date>
                    <gmd:CI_Date>
                        <gmd:date>
                            <xsl:variable name="embargoDate" select="concat(
                                    EmbargoDate/@Year, '-',
                                    format-number(number(EmbargoDate/@Month),'00'), '-',
                                    format-number(number(EmbargoDate/@Day),'00')     
                                )" />
                            <gco:Date>
                                <xsl:value-of select="$embargoDate"/>
                            </gco:Date>
                        </gmd:date>
                        <gmd:dateType>
                            <gmd:CI_DateTypeCode codeList="http://www.isotc211.org/2005/resources/Codelist/gmxCodelists.xml#CI_DateTypeCode" codeListValue="publication">publication</gmd:CI_DateTypeCode>
                        </gmd:dateType>
                    </gmd:CI_Date>
                </gmd:date>
            </xsl:when>
            <xsl:when test="*[local-name() = 'ServerDatePublished']">
                <gmd:date>
                    <gmd:CI_Date>
                        <gmd:date>
                            <xsl:variable name="publicationDate" select="concat(
                                    ServerDatePublished/@Year, '-',
                                    format-number(number(ServerDatePublished/@Month),'00'), '-',
                                    format-number(number(ServerDatePublished/@Day),'00'), 'T',
                                    format-number(number(ServerDatePublished/@Hour),'00'), ':',
                                    format-number(number(ServerDatePublished/@Minute),'00'), ':',
                                    format-number(number(ServerDatePublished/@Second),'00'), 'Z'   
                                )" />
                            <gco:DateTime>
                                <xsl:value-of select="$publicationDate"/>
                            </gco:DateTime>
                        </gmd:date>
                        <gmd:dateType>
                            <gmd:CI_DateTypeCode codeList="http://www.isotc211.org/2005/resources/Codelist/gmxCodelists.xml#CI_DateTypeCode" codeListValue="publication">publication</gmd:CI_DateTypeCode>
                        </gmd:dateType>
                    </gmd:CI_Date>
                </gmd:date>
            </xsl:when>
            <xsl:otherwise>
                <gmd:date gco:nilReason="missing"/>
            </xsl:otherwise>
        </xsl:choose>
        <!-- the following test is designed to filter out update-metadata dates if these follow the convention
             that metadata update date strings are prefixed with 'metadata'-->
        <xsl:if test="@ServerDateModified">
            <gmd:date>
                <gmd:CI_Date>
                    <gmd:date>
                        <xsl:variable name="updateDate" select="normalize-space(@ServerDateModified)"/>
                        <!-- ieda dataCite labels metadata update date in the date string to avoid ambiguity -->
                        <xsl:choose>
                            <xsl:when test="$updateDate != ''">
                                <xsl:choose>
                                    <xsl:when test="contains($updateDate, 'T')">
                                        <gco:DateTime>
                                            <xsl:value-of select="$updateDate"/>
                                        </gco:DateTime>
                                    </xsl:when>
                                    <xsl:when test="contains($updateDate, ' ')">
                                        <gco:DateTime>
                                            <xsl:value-of select="translate($updateDate, ' ', 'T')"/>
                                        </gco:DateTime>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <gco:Date>
                                            <xsl:value-of select="$updateDate"/>
                                        </gco:Date>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="gco:nilReason">
                                    <xsl:value-of select="string('missing')"/>
                                </xsl:attribute>
                            </xsl:otherwise>
                        </xsl:choose>
                    </gmd:date>
                    <gmd:dateType>
                        <gmd:CI_DateTypeCode codeList="http://www.isotc211.org/2005/resources/Codelist/gmxCodelists.xml#CI_DateTypeCode" codeListValue="revision">revision</gmd:CI_DateTypeCode>
                    </gmd:dateType>
                </gmd:CI_Date>
            </gmd:date>
        </xsl:if>  
    </xsl:template> 
    
    <xsl:template name="ci_responsibleparty">
        <xsl:param name="individual"/>   
        <xsl:param name="httpuri"/>    
        <xsl:param name="personidtype"/>
        <xsl:param name="organisation"/>       
        <xsl:param name="position"/>
        <xsl:param name="role"/>
        <xsl:param name="email"/>
        <xsl:param name="datacite-creatorname"/>
        <!-- <xsl:variable name="datacite-creatorname" select="//*[name() = 'creatorName'][@nameType = 'Organizational']"/> -->
        <gmd:CI_ResponsibleParty>
            <!-- stower 2022, wenn Autor oder Creator nur Organisation und Individual = missing -->
            <xsl:if test="$individual != ''">
                <xsl:choose>
                    <xsl:when test="$individual = 'missing'">
                        <gmd:individualName gco:nilReason="missing"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <gmd:individualName>
                            <gco:CharacterString>
                                <xsl:value-of select="$individual"/>
                            </gco:CharacterString>
                        </gmd:individualName>
                    </xsl:otherwise>
                </xsl:choose>	
            </xsl:if>   
            <!-- stower 2022, wenn Autor oder Creator nur Organisation -->
            <xsl:if test="$individual = 'missing' or ''">
                <gmd:organisationName>
                    <gco:CharacterString>
                        <xsl:value-of select="$datacite-creatorname"/>
                    </gco:CharacterString>
                </gmd:organisationName>
            </xsl:if>	
            
            <xsl:if test="$organisation != ''">
                <gmd:organisationName>
                    <gco:CharacterString>
                        <xsl:value-of select="$organisation"/>
                    </gco:CharacterString>
                </gmd:organisationName>
            </xsl:if>
            
            <xsl:if test="$position != ''">
                <gmd:positionName>
                    <gco:CharacterString>
                        <xsl:value-of select="$position"/>
                    </gco:CharacterString>
                </gmd:positionName>
            </xsl:if>
            
            <gmd:contactInfo>
                <gmd:CI_Contact>
                    <!-- gmd:address -->
                    <gmd:address>
                        <gmd:CI_Address>
                            <gmd:electronicMailAddress>
                                <xsl:choose>
                                    <xsl:when test="$email != ''">
                                        <gco:CharacterString>
                                            <xsl:value-of select="$email"/>
                                        </gco:CharacterString>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <!-- stower 2022 -->
                                        <gco:CharacterString>
                                            <xsl:value-of select="string('repository@geologie.ac.at')"/>
                                        </gco:CharacterString>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </gmd:electronicMailAddress>
                        </gmd:CI_Address>
                    </gmd:address>
                    <xsl:if test="$httpuri != ''">
                        <gmd:onlineResource>
                            <gmd:CI_OnlineResource>
                                <gmd:linkage>
                                    <gmd:URL>
                                        <xsl:value-of select="$httpuri"/>
                                    </gmd:URL>
                                </gmd:linkage>
                                <gmd:protocol>
                                    <gco:CharacterString>
                                        <xsl:value-of select="$personidtype"/>
                                    </gco:CharacterString>
                                </gmd:protocol>
                            </gmd:CI_OnlineResource>
                        </gmd:onlineResource>
                    </xsl:if>                    
                </gmd:CI_Contact>
            </gmd:contactInfo>
            
            <!-- <gmd:role> -->
            <gmd:role>
                <gmd:CI_RoleCode>
                    <xsl:attribute name="codeList">http://www.isotc211.org/2005/resources/Codelist/gmxCodelists.xml#CI_RoleCode</xsl:attribute>
                    <xsl:attribute name="codeListValue">
                        <xsl:value-of select="$role"/>
                    </xsl:attribute>
                    <xsl:value-of select="$role"/>
                </gmd:CI_RoleCode>
            </gmd:role>
        </gmd:CI_ResponsibleParty>
    </xsl:template>
    
    <!-- <gmd:pointOfContact> -->
    <xsl:template name="pointofcontact_custodian">
        <gmd:pointOfContact>
            <gmd:CI_ResponsibleParty>
                <gmd:individualName>
                    <gco:CharacterString>Tethys RDR</gco:CharacterString>
                </gmd:individualName>
                <gmd:organisationName>
                    <gco:CharacterString>Tethys RDR</gco:CharacterString>
                </gmd:organisationName>
                <gmd:contactInfo>
                    <gmd:CI_Contact>
                        <gmd:address>
                            <gmd:CI_Address>
                                <gmd:electronicMailAddress>
                                    <gco:CharacterString>repository@geologie.ac.at</gco:CharacterString>
                                </gmd:electronicMailAddress>
                            </gmd:CI_Address>
                        </gmd:address>
                        <gmd:onlineResource>
                            <gmd:CI_OnlineResource>
                                <gmd:linkage>
                                    <gmd:URL>https://www.tethys.at/</gmd:URL>
                                </gmd:linkage>
                            </gmd:CI_OnlineResource>
                        </gmd:onlineResource>
                    </gmd:CI_Contact>
                </gmd:contactInfo>
                <!-- <gmd:role>
                     <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/Codelist/gmxCodelists.xml#CI_RoleCode" codeListValue="pointOfContact">pointOfContact</gmd:CI_RoleCode>
                     </gmd:role> -->
                <gmd:role>
                    <gmd:CI_RoleCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/gmxCodelists.xml#CI_RoleCode" codeListValue="custodian">custodian</gmd:CI_RoleCode>
                </gmd:role>
            </gmd:CI_ResponsibleParty>
        </gmd:pointOfContact>
    </xsl:template>
    
    <!-- <gmd:pointOfContact> -->
    <xsl:template name="pointofcontact_originator">
        <gmd:pointOfContact>
            <gmd:CI_ResponsibleParty>
                <gmd:individualName>
                    <gco:CharacterString>Tethys RDR</gco:CharacterString>
                </gmd:individualName>
                <gmd:organisationName>
                    <gco:CharacterString>Tethys RDR</gco:CharacterString>
                </gmd:organisationName>
                <gmd:contactInfo>
                    <gmd:CI_Contact>
                        <gmd:address>
                            <gmd:CI_Address>
                                <gmd:electronicMailAddress>
                                    <gco:CharacterString>repository@geologie.ac.at</gco:CharacterString>
                                </gmd:electronicMailAddress>
                            </gmd:CI_Address>
                        </gmd:address>
                        <gmd:onlineResource>
                            <gmd:CI_OnlineResource>
                                <gmd:linkage>
                                    <gmd:URL>https://www.tethys.at/</gmd:URL>
                                </gmd:linkage>
                            </gmd:CI_OnlineResource>
                        </gmd:onlineResource>
                    </gmd:CI_Contact>
                </gmd:contactInfo>
                <!-- <gmd:role>
                     <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/Codelist/gmxCodelists.xml#CI_RoleCode" codeListValue="pointOfContact">pointOfContact</gmd:CI_RoleCode>
                     </gmd:role> -->
                <gmd:role>
                    <gmd:CI_RoleCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/gmxCodelists.xml#CI_RoleCode" codeListValue="originator">originator</gmd:CI_RoleCode>
                </gmd:role>
            </gmd:CI_ResponsibleParty>
        </gmd:pointOfContact>
    </xsl:template>
    
    <!-- gmd:graphicOverview - retrieves tethys logo as browse graphic, stower 2022 -->
    <xsl:template name="browseGraphictethys">
        <gmd:graphicOverview>
            <gmd:MD_BrowseGraphic>
                <gmd:fileName>
                    <gco:CharacterString>https://tethys.at/assets/TETHYS-Logo.svg</gco:CharacterString>
                </gmd:fileName>
                <gmd:fileDescription>
                    <gco:CharacterString>TETHYS RDR</gco:CharacterString>
                </gmd:fileDescription>
            </gmd:MD_BrowseGraphic>
        </gmd:graphicOverview>
    </xsl:template>
    
    <!-- gmd:descriptiveKeywords - retrieves non-thesaurus keywords -->
    <xsl:template name="freekeywords">
        <xsl:if test="//*[name() = 'Subject' and normalize-space(@Type) = 'Uncontrolled']">
            <gmd:descriptiveKeywords>
                <gmd:MD_Keywords>
                    <xsl:for-each select="*[name() = 'Subject' and normalize-space(@Type) = 'Uncontrolled']">                    
                        <gmd:keyword>
                            <gco:CharacterString>
                                <xsl:value-of select="normalize-space(@Value)"/>
                            </gco:CharacterString>
                        </gmd:keyword>
                    </xsl:for-each>
                    <gmd:type>
                        <gmd:MD_KeywordTypeCode xmlns="http://www.isotc211.org/2005/gmd" codeList="https://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_KeywordTypeCode" codeListValue="theme">theme</gmd:MD_KeywordTypeCode>
                    </gmd:type>                  
                </gmd:MD_Keywords>
            </gmd:descriptiveKeywords>
        </xsl:if>
    </xsl:template>
    
    <!-- gmd:descriptiveKeywords - for datacenter -->
    <xsl:template name="datacenter_keyword">
        <gmd:descriptiveKeywords>
            <gmd:MD_Keywords>
                <gmd:keyword>
                    <gco:CharacterString>
                        <xsl:value-of select="$datacentre"/>
                    </gco:CharacterString>
                </gmd:keyword>
                <gmd:type>
                    <gmd:MD_KeywordTypeCode codeList="http://www.ngdc.noaa.gov/metadata/published/xsd/schema/resources/Codelist/gmxCodelists.xml#MD_KeywordTypeCode" codeListValue="dataCentre">Tethys RDR</gmd:MD_KeywordTypeCode>
                </gmd:type>
            </gmd:MD_Keywords>
        </gmd:descriptiveKeywords>
    </xsl:template>
    
    <!-- <gmd:extent> -->
    <xsl:template name="spatialcoverage">
        <gmd:extent>
            <gmd:EX_Extent>
                <xsl:if test="//*[name() = 'Coverage']">
                    <!-- <xsl:variable name="thebox" select="//*[name() = 'Coverage']"/> -->
                    <xsl:variable name="sLat">
                        <xsl:choose>                          
                            <xsl:when test="string(Coverage/@YMin) != ''">   
                                <xsl:value-of select="number(Coverage/@YMin)"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="''"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:variable name="wLong">
                        <xsl:choose>                          
                            <xsl:when test="string(Coverage/@XMin) != ''">   
                                <xsl:value-of select="number(Coverage/@XMin)"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="''"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:variable name="nLat">
                        <xsl:choose>                          
                            <xsl:when test="string(Coverage/@YMax) != ''">   
                                <xsl:value-of select="number(Coverage/@YMax)"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="''"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:variable name="eLong">
                        <xsl:choose>                          
                            <xsl:when test="string(Coverage/@XMax) != ''">   
                                <xsl:value-of select="number(Coverage/@XMax)"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="''"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:if test="
                        string($nLat) != 'NaN' and string($wLong) != 'NaN' and
                        string($sLat) != 'NaN' and string($eLong) != 'NaN'">
                        <!-- <xsl:value-of select="$nLat"/> -->
                        <xsl:choose>
                            <!-- check for degenerate bbox -->
                            <xsl:when test="$nLat = $sLat and $eLong = $wLong">
                                <gmd:geographicElement>
                                    <gmd:EX_BoundingPolygon>
                                        <gmd:polygon>
                                            <gml:Point>
                                                <!-- generate a unique id of the source xml node -->
                                                <xsl:attribute name="gml:id">
                                                    <xsl:value-of select="generate-id(.)"/>
                                                </xsl:attribute>
                                                <gml:pos>
                                                    <xsl:value-of select="$sLat"/>
                                                    <xsl:text> </xsl:text>
                                                    <xsl:value-of select="$wLong"/>
                                                </gml:pos>
                                            </gml:Point>
                                        </gmd:polygon>
                                    </gmd:EX_BoundingPolygon>
                                </gmd:geographicElement>
                            </xsl:when>
                            <!-- check to see if box crosses 180 with west side either in east long (positive) or <-180, or east side  
                                 >180. If so, create two bounding box geographicElements-->
                            <xsl:when test="($wLong &gt;0 and $eLong &lt;0)">
                                <!-- use east longitude coordinates -->
                                <gmd:geographicElement>
                                    <gmd:EX_GeographicBoundingBox>
                                        <gmd:westBoundLongitude>
                                            <gco:Decimal>
                                                <xsl:value-of select="$wLong"/>
                                            </gco:Decimal>
                                        </gmd:westBoundLongitude>
                                        <gmd:eastBoundLongitude>
                                            <gco:Decimal>
                                                <xsl:value-of select="180"/>
                                            </gco:Decimal>
                                        </gmd:eastBoundLongitude>
                                        <gmd:southBoundLatitude>
                                            <gco:Decimal>
                                                <xsl:choose>
                                                    <xsl:when test="number($sLat) &lt; number($nLat)">
                                                        <xsl:value-of select="$sLat"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="$nLat"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </gco:Decimal>
                                        </gmd:southBoundLatitude>
                                        <gmd:northBoundLatitude>
                                            <gco:Decimal>
                                                <xsl:choose>
                                                    <xsl:when test="number($sLat) &lt; number($nLat)">
                                                        <xsl:value-of select="$nLat"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="$sLat"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </gco:Decimal>
                                        </gmd:northBoundLatitude>
                                    </gmd:EX_GeographicBoundingBox>
                                </gmd:geographicElement>
                                <gmd:geographicElement>
                                    <gmd:EX_GeographicBoundingBox>
                                        <gmd:westBoundLongitude>
                                            <gco:Decimal>
                                                <xsl:value-of select="-180"/>
                                            </gco:Decimal>
                                        </gmd:westBoundLongitude>
                                        <gmd:eastBoundLongitude>
                                            <gco:Decimal>
                                                <xsl:value-of select="$eLong"/>
                                            </gco:Decimal>
                                        </gmd:eastBoundLongitude>
                                        <gmd:southBoundLatitude>
                                            <gco:Decimal>
                                                <xsl:choose>
                                                    <xsl:when test="number($sLat) &lt; number($nLat)">
                                                        <xsl:value-of select="$sLat"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="$nLat"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </gco:Decimal>
                                        </gmd:southBoundLatitude>
                                        <gmd:northBoundLatitude>
                                            <gco:Decimal>
                                                <xsl:choose>
                                                    <xsl:when test="number($sLat) &lt; number($nLat)">
                                                        <xsl:value-of select="$nLat"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="$sLat"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </gco:Decimal>
                                        </gmd:northBoundLatitude>
                                    </gmd:EX_GeographicBoundingBox>
                                </gmd:geographicElement>
                            </xsl:when>
                            <xsl:otherwise>
                                <gmd:geographicElement>
                                    <gmd:EX_GeographicBoundingBox>
                                        <gmd:extentTypeCode>
                                            <gco:Boolean>true</gco:Boolean>
                                        </gmd:extentTypeCode>
                                        <gmd:westBoundLongitude>
                                            <gco:Decimal>                                               
                                                <xsl:value-of select="$wLong"/>                                               
                                            </gco:Decimal>
                                        </gmd:westBoundLongitude>
                                        <gmd:eastBoundLongitude>
                                            <gco:Decimal>                                                
                                                <xsl:value-of select="$eLong"/>                                              
                                            </gco:Decimal>
                                        </gmd:eastBoundLongitude>
                                        <gmd:southBoundLatitude>
                                            <gco:Decimal>
                                                <xsl:choose>
                                                    <xsl:when test="number($sLat) &lt; number($nLat)">
                                                        <xsl:value-of select="$sLat"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="$nLat"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </gco:Decimal>
                                        </gmd:southBoundLatitude>
                                        <gmd:northBoundLatitude>
                                            <gco:Decimal>
                                                <xsl:choose>
                                                    <xsl:when test="number($sLat) &lt; number($nLat)">
                                                        <xsl:value-of select="$nLat"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="$sLat"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </gco:Decimal>
                                        </gmd:northBoundLatitude>
                                    </gmd:EX_GeographicBoundingBox>
                                </gmd:geographicElement>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                </xsl:if>
            </gmd:EX_Extent>
        </gmd:extent>
    </xsl:template>
    
    <!-- <gmd:onLine> -->
    <xsl:template name="datacite_identifier">
        <xsl:for-each select="*[name() = 'Identifier']">
            <xsl:variable name="identifier" select="."/>
            <xsl:if test="starts-with($identifier/@Value, 'doi:') or $identifier/@Type = 'Doi' or starts-with($identifier/@Value, 'http://')">
                <gmd:onLine>
                    <gmd:CI_OnlineResource>
                        <gmd:linkage>
                            <gmd:URL>
                                <xsl:choose>
                                    <xsl:when test="starts-with($identifier/@Value, 'doi:')">
                                        <!-- stower http auf https gesetzt -->
                                        <xsl:value-of select="concat('http://dx.doi.org/', substring-after($identifier/@Value, 'doi:'))"/>
                                    </xsl:when>
                                    <xsl:when test="$identifier/@Type = 'Doi'">
                                        <xsl:value-of select="concat('http://dx.doi.org/', normalize-space($identifier/@Value))"/>
                                    </xsl:when>
                                    <xsl:when test="starts-with($identifier/@Value, 'http://')">
                                        <xsl:value-of select="normalize-space($identifier/@Value)"/>
                                    </xsl:when>
                                </xsl:choose>
                            </gmd:URL>
                        </gmd:linkage>
                        <gmd:protocol>
                            <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
                        </gmd:protocol>
                        <gmd:name>
                            <gco:CharacterString>Landing Page</gco:CharacterString>
                        </gmd:name>
                        <gmd:description>
                            <gco:CharacterString>
                                <xsl:value-of select="normalize-space(string(
                                            'Link to DOI landing page or data facility landing page if no DOI is assigned.'))"/>
                            </gco:CharacterString>
                        </gmd:description>
                        <gmd:function>
                            <gmd:CI_OnLineFunctionCode codeList="http://www.isotc211.org/2005/resources/Codelist/gmxCodelists.xml#CI_OnLineFunctionCode" codeListValue="information">information</gmd:CI_OnLineFunctionCode>
                        </gmd:function>
                    </gmd:CI_OnlineResource>
                </gmd:onLine>
            </xsl:if>  
        </xsl:for-each>                   
    </xsl:template>
    
    <!-- <gmd:onLine> -->
    <xsl:template name="alternate_identifier">
        <xsl:if test="@landingpage != '' and starts-with(normalize-space(string(@landingpage)), 'http')">
            <gmd:onLine>
                <gmd:CI_OnlineResource>
                    <gmd:linkage>
                        <gmd:URL>
                            <xsl:value-of select="string(@landingpage)"/>
                        </gmd:URL>
                    </gmd:linkage>
                    <gmd:protocol>
                        <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
                    </gmd:protocol>
                    <gmd:name>
                        <gco:CharacterString>
                            <xsl:value-of select="string('url')"/>
                        </gco:CharacterString>
                    </gmd:name>
                    <gmd:description>
                        <gco:CharacterString>Link to a web page related to the resource.</gco:CharacterString>
                    </gmd:description>
                    <gmd:function>
                        <gmd:CI_OnLineFunctionCode codeList="http://www.isotc211.org/2005/resources/Codelist/gmxCodelists.xml#CI_OnLineFunctionCode" codeListValue="information">information</gmd:CI_OnLineFunctionCode>
                    </gmd:function>
                </gmd:CI_OnlineResource>
            </gmd:onLine>
        </xsl:if>
    </xsl:template>
    
    <!-- <gmd:dataQualityInfo> -->
    <xsl:template name="data_quality">
        <gmd:dataQualityInfo>
            <gmd:DQ_DataQuality>
                <gmd:scope>
                    <gmd:DQ_Scope>
                        <gmd:level>
                            <gmd:MD_ScopeCode xmlns="http://www.isotc211.org/2005/gmd" codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_ScopeCode" codeListValue="dataset" />
                        </gmd:level>
                        <gmd:levelDescription xmlns:gco="http://www.isotc211.org/2005/gco" gco:nilReason="inapplicable" />
                    </gmd:DQ_Scope>
                </gmd:scope>
                <gmd:report>
                    <gmd:DQ_DomainConsistency>
                        <gmd:result>
                            <gmd:DQ_ConformanceResult>
                                <gmd:specification>
                                    <gmd:CI_Citation>
                                        <gmd:title>
                                            <gco:CharacterString xmlns:gco="http://www.isotc211.org/2005/gco">VERORDNUNG (EG) Nr. 1089/2010 DER KOMMISSION vom 23. November 2010 zur Durchführung der Richtlinie 2007/2/EG des Europäischen Parlaments und des Rates hinsichtlich der Interoperabilität von Geodatensätzen und -diensten</gco:CharacterString>
                                        </gmd:title>
                                        <gmd:date>
                                            <gmd:CI_Date>
                                                <gmd:date>
                                                    <gco:Date xmlns:gco="http://www.isotc211.org/2005/gco">2010-12-08</gco:Date>
                                                </gmd:date>
                                                <gmd:dateType>
                                                    <gmd:CI_DateTypeCode xmlns="http://www.isotc211.org/2005/gmd" codeList="https://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_DateTypeCode" codeListValue="publication" />
                                                </gmd:dateType>
                                            </gmd:CI_Date>
                                        </gmd:date>
                                    </gmd:CI_Citation>
                                </gmd:specification>
                                <gmd:explanation>
                                    <gco:CharacterString xmlns:gco="http://www.isotc211.org/2005/gco">Datenstruktur entspricht INSPIRE</gco:CharacterString>
                                </gmd:explanation>
                                <gmd:pass>
                                    <gco:Boolean xmlns:gco="http://www.isotc211.org/2005/gco">true</gco:Boolean>
                                </gmd:pass>
                            </gmd:DQ_ConformanceResult>
                        </gmd:result>
                    </gmd:DQ_DomainConsistency>
                </gmd:report>
                <gmd:lineage>
                    <gmd:LI_Lineage>
                        <gmd:statement>
                            <gco:CharacterString xmlns:gco="http://www.isotc211.org/2005/gco">Digitalisierung</gco:CharacterString>
                        </gmd:statement>
                    </gmd:LI_Lineage>
                </gmd:lineage>
            </gmd:DQ_DataQuality>
        </gmd:dataQualityInfo>
    </xsl:template>
    
    <!-- <gmd:distributor> -->
    <xsl:variable name="distributorOrganisation" select="string('Geological Survey of Austria')"/>
    <xsl:variable name="distributorContactEmail" select="string('repository@geologie.ac.at')"/>
    <xsl:template name="distributor">
        <gmd:distributor>
            <gmd:MD_Distributor>
                <gmd:distributorContact>
                    <gmd:CI_ResponsibleParty>
                        <gmd:organisationName>
                            <gco:CharacterString>
                                <xsl:value-of select="$distributorOrganisation"/>
                            </gco:CharacterString>
                        </gmd:organisationName>
                        <gmd:contactInfo>
                            <gmd:CI_Contact>
                                <gmd:address>
                                    <gmd:CI_Address>
                                        <gmd:electronicMailAddress>
                                            <gco:CharacterString>
                                                <xsl:value-of select="$distributorContactEmail"/>
                                            </gco:CharacterString>
                                        </gmd:electronicMailAddress>
                                    </gmd:CI_Address>
                                </gmd:address>
                            </gmd:CI_Contact>
                        </gmd:contactInfo>
                        <gmd:role>
                            <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/Codelist/gmxCodelists.xml#CI_RoleCode" codeListValue="distributor" codeSpace="ISOTC211/19115">distributor</gmd:CI_RoleCode>
                        </gmd:role>
                    </gmd:CI_ResponsibleParty>
                </gmd:distributorContact>
            </gmd:MD_Distributor>
        </gmd:distributor>
    </xsl:template>
    
    
    <xsl:variable name="maintenanceContactID" select="string('https://www.re3data.org/repository/r3d100013400')"/>
    <xsl:variable name="maintenanceContactName" select="string('Tethys RDR')"/>
    <xsl:variable name="maintenanceContactEmail" select="string('repository@geologie.ac.at')"/>
    <xsl:template name="metadata_maintenance">
        <gmd:metadataMaintenance>
            <gmd:MD_MaintenanceInformation>              
                <gmd:maintenanceAndUpdateFrequency>
                    <gmd:MD_MaintenanceFrequencyCode codeList="http://schemas.opengis.net/iso/19139/20070417/resources/codelist/gmxCodelists.xml#MD_MaintenanceFrequencyCode" codeListValue="asNeeded">asNeeded</gmd:MD_MaintenanceFrequencyCode>
                </gmd:maintenanceAndUpdateFrequency>
                <gmd:contact>
                    <xsl:if test="$maintenanceContactID != ''">
                        <xsl:attribute name="xlink:href">
                            <xsl:value-of select="$maintenanceContactID"/>
                        </xsl:attribute>
                    </xsl:if>
                    <gmd:CI_ResponsibleParty>
                        <gmd:individualName>
                            <gco:CharacterString>
                                <xsl:value-of select="$maintenanceContactName"/>
                            </gco:CharacterString>
                        </gmd:individualName>
                        <gmd:contactInfo>
                            <gmd:CI_Contact>
                                <gmd:address>
                                    <gmd:CI_Address>
                                        <gmd:electronicMailAddress>
                                            <gco:CharacterString>
                                                <xsl:value-of select="$maintenanceContactEmail"/>
                                            </gco:CharacterString>
                                        </gmd:electronicMailAddress>
                                    </gmd:CI_Address>
                                </gmd:address>
                            </gmd:CI_Contact>
                        </gmd:contactInfo>
                        <gmd:role>
                            <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/Codelist/gmxCodelists.xml#CI_RoleCode" codeListValue="processor">processor</gmd:CI_RoleCode>
                        </gmd:role>
                    </gmd:CI_ResponsibleParty>
                </gmd:contact>
            </gmd:MD_MaintenanceInformation>
        </gmd:metadataMaintenance>    
    </xsl:template>
    
    
</xsl:stylesheet>